package com.ruoyi.menugoods.service;

import java.util.List;
import com.ruoyi.menugoods.domain.SysMenugoods;

/**
 * 菜单中商品Service接口
 * 
 * @author yhh
 * @date 2020-12-23
 */
public interface ISysMenugoodsService 
{
    /**
     * 查询菜单中商品
     * 
     * @param menuId 菜单中商品ID
     * @return 菜单中商品
     */
    public SysMenugoods selectSysMenugoodsById(Long menuId);

    /**
     * 查询菜单中商品列表
     * 
     * @param sysMenugoods 菜单中商品
     * @return 菜单中商品集合
     */
    public List<SysMenugoods> selectSysMenugoodsList(SysMenugoods sysMenugoods);

    /**
     * 新增菜单中商品
     * 
     * @param sysMenugoods 菜单中商品
     * @return 结果
     */
    public int insertSysMenugoods(SysMenugoods sysMenugoods);

    /**
     * 修改菜单中商品
     * 
     * @param sysMenugoods 菜单中商品
     * @return 结果
     */
    public int updateSysMenugoods(SysMenugoods sysMenugoods);

    /**
     * 批量删除菜单中商品
     * 
     * @param menuIds 需要删除的菜单中商品ID
     * @return 结果
     */
    public int deleteSysMenugoodsByIds(Long[] menuIds);

    /**
     * 删除菜单中商品信息
     * 
     * @param menuId 菜单中商品ID
     * @return 结果
     */
    public int deleteSysMenugoodsById(Long menuId);
}
