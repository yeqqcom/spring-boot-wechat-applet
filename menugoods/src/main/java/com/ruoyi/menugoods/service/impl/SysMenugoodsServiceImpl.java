package com.ruoyi.menugoods.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.menugoods.mapper.SysMenugoodsMapper;
import com.ruoyi.menugoods.domain.SysMenugoods;
import com.ruoyi.menugoods.service.ISysMenugoodsService;

/**
 * 菜单中商品Service业务层处理
 * 
 * @author yhh
 * @date 2020-12-23
 */
@Service
public class SysMenugoodsServiceImpl implements ISysMenugoodsService 
{
    @Autowired
    private SysMenugoodsMapper sysMenugoodsMapper;

    /**
     * 查询菜单中商品
     * 
     * @param menuId 菜单中商品ID
     * @return 菜单中商品
     */
    @Override
    public SysMenugoods selectSysMenugoodsById(Long menuId)
    {
        return sysMenugoodsMapper.selectSysMenugoodsById(menuId);
    }

    /**
     * 查询菜单中商品列表
     * 
     * @param sysMenugoods 菜单中商品
     * @return 菜单中商品
     */
    @Override
    public List<SysMenugoods> selectSysMenugoodsList(SysMenugoods sysMenugoods)
    {
        return sysMenugoodsMapper.selectSysMenugoodsList(sysMenugoods);
    }

    /**
     * 新增菜单中商品
     * 
     * @param sysMenugoods 菜单中商品
     * @return 结果
     */
    @Override
    public int insertSysMenugoods(SysMenugoods sysMenugoods)
    {
        return sysMenugoodsMapper.insertSysMenugoods(sysMenugoods);
    }

    /**
     * 修改菜单中商品
     * 
     * @param sysMenugoods 菜单中商品
     * @return 结果
     */
    @Override
    public int updateSysMenugoods(SysMenugoods sysMenugoods)
    {
        return sysMenugoodsMapper.updateSysMenugoods(sysMenugoods);
    }

    /**
     * 批量删除菜单中商品
     * 
     * @param menuIds 需要删除的菜单中商品ID
     * @return 结果
     */
    @Override
    public int deleteSysMenugoodsByIds(Long[] menuIds)
    {
        return sysMenugoodsMapper.deleteSysMenugoodsByIds(menuIds);
    }

    /**
     * 删除菜单中商品信息
     * 
     * @param menuId 菜单中商品ID
     * @return 结果
     */
    @Override
    public int deleteSysMenugoodsById(Long menuId)
    {
        return sysMenugoodsMapper.deleteSysMenugoodsById(menuId);
    }
}
