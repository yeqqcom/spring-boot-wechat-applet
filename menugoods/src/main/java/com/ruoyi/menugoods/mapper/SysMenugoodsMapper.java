package com.ruoyi.menugoods.mapper;

import java.util.List;
import com.ruoyi.menugoods.domain.SysMenugoods;

/**
 * 菜单中商品Mapper接口
 * 
 * @author yhh
 * @date 2020-12-23
 */
public interface SysMenugoodsMapper 
{
    /**
     * 查询菜单中商品
     * 
     * @param menuId 菜单中商品ID
     * @return 菜单中商品
     */
    public SysMenugoods selectSysMenugoodsById(Long menuId);

    /**
     * 查询菜单中商品列表
     * 
     * @param sysMenugoods 菜单中商品
     * @return 菜单中商品集合
     */
    public List<SysMenugoods> selectSysMenugoodsList(SysMenugoods sysMenugoods);

    /**
     * 新增菜单中商品
     * 
     * @param sysMenugoods 菜单中商品
     * @return 结果
     */
    public int insertSysMenugoods(SysMenugoods sysMenugoods);

    /**
     * 修改菜单中商品
     * 
     * @param sysMenugoods 菜单中商品
     * @return 结果
     */
    public int updateSysMenugoods(SysMenugoods sysMenugoods);

    /**
     * 删除菜单中商品
     * 
     * @param menuId 菜单中商品ID
     * @return 结果
     */
    public int deleteSysMenugoodsById(Long menuId);

    /**
     * 批量删除菜单中商品
     * 
     * @param menuIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysMenugoodsByIds(Long[] menuIds);
}
