package com.ruoyi.menugoods.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 菜单中商品对象 sys_menugoods
 * 
 * @author yhh
 * @date 2020-12-23
 */
public class SysMenugoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 菜单编号 */
    @Excel(name = "菜单编号")
    private Long menuId;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private Long goodsId;

    public void setMenuId(Long menuId) 
    {
        this.menuId = menuId;
    }

    public Long getMenuId() 
    {
        return menuId;
    }
    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("menuId", getMenuId())
            .append("goodsId", getGoodsId())
            .toString();
    }
}
