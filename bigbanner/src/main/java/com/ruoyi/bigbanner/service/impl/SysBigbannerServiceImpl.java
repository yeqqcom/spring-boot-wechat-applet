package com.ruoyi.bigbanner.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.bigbanner.mapper.SysBigbannerMapper;
import com.ruoyi.bigbanner.domain.SysBigbanner;
import com.ruoyi.bigbanner.service.ISysBigbannerService;

/**
 * 画报Service业务层处理
 * 
 * @author yhh
 * @date 2020-12-23
 */
@Service
public class SysBigbannerServiceImpl implements ISysBigbannerService 
{
    @Autowired
    private SysBigbannerMapper sysBigbannerMapper;

    /**
     * 查询画报
     * 
     * @param bannerId 画报ID
     * @return 画报
     */
    @Override
    public SysBigbanner selectSysBigbannerById(Long bannerId)
    {
        return sysBigbannerMapper.selectSysBigbannerById(bannerId);
    }

    /**
     * 查询画报列表
     * 
     * @param sysBigbanner 画报
     * @return 画报
     */
    @Override
    public List<SysBigbanner> selectSysBigbannerList(SysBigbanner sysBigbanner)
    {
        return sysBigbannerMapper.selectSysBigbannerList(sysBigbanner);
    }
    @Override
    public List<String> selectSysBigbannerByState(String state){
        return (List<String>) sysBigbannerMapper.selectSysBigbannerByState(state);
    }
    /**
     * 新增画报
     * 
     * @param sysBigbanner 画报
     * @return 结果
     */
    @Override
    public int insertSysBigbanner(SysBigbanner sysBigbanner)
    {
        return sysBigbannerMapper.insertSysBigbanner(sysBigbanner);
    }

    /**
     * 修改画报
     * 
     * @param sysBigbanner 画报
     * @return 结果
     */
    @Override
    public int updateSysBigbanner(SysBigbanner sysBigbanner)
    {
        return sysBigbannerMapper.updateSysBigbanner(sysBigbanner);
    }

    /**
     * 批量删除画报
     * 
     * @param bannerIds 需要删除的画报ID
     * @return 结果
     */
    @Override
    public int deleteSysBigbannerByIds(Long[] bannerIds)
    {
        return sysBigbannerMapper.deleteSysBigbannerByIds(bannerIds);
    }

    /**
     * 删除画报信息
     * 
     * @param bannerId 画报ID
     * @return 结果
     */
    @Override
    public int deleteSysBigbannerById(Long bannerId)
    {

        return sysBigbannerMapper.deleteSysBigbannerById(bannerId);
    }
    /**
     * 获取可用画报数
     *
     * @return 结果
     */
    @Override
    public int countShowBanner(){
        return sysBigbannerMapper.countShowBanner();
    };
}
