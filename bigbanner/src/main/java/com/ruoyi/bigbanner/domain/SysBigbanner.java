package com.ruoyi.bigbanner.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 画报对象 sys_bigbanner
 * 
 * @author yhh
 * @date 2020-12-23
 */
public class SysBigbanner extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 画报编号 */
    @Excel(name = "画报编号")
    private Long bannerId;

    /** 图片 */
    @Excel(name = "图片")
    private String pic;

    /** 状态 */
    @Excel(name = "状态")
    private String state;

    public void setBannerId(Long bannerId) 
    {
        this.bannerId = bannerId;
    }

    public Long getBannerId() 
    {
        return bannerId;
    }

    public void setPic(String pic) 
    {
        this.pic = pic;
    }

    public String getPic() 
    {
        return pic;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("bannerId", getBannerId())
            .append("pic", getPic())
            .append("state", getState())
            .toString();
    }
}
