package com.ruoyi.bigbanner.mapper;

import java.util.List;
import com.ruoyi.bigbanner.domain.SysBigbanner;

/**
 * 画报Mapper接口
 * 
 * @author yhh
 * @date 2020-12-23
 */
public interface SysBigbannerMapper 
{
    /**
     * 查询画报
     * 
     * @param bannerId 画报ID
     * @return 画报
     */
    public SysBigbanner selectSysBigbannerById(Long bannerId);

    /**
     * 查询启用的画报地址
     *
     * @param state 画报状态
     * @return 画报
     */
    public List<String> selectSysBigbannerByState(String state);
    /**
     * 查询画报列表
     * 
     * @param sysBigbanner 画报
     * @return 画报集合
     */
    public List<SysBigbanner> selectSysBigbannerList(SysBigbanner sysBigbanner);

    /**
     * 新增画报
     * 
     * @param sysBigbanner 画报
     * @return 结果
     */
    public int insertSysBigbanner(SysBigbanner sysBigbanner);

    /**
     * 修改画报
     * 
     * @param sysBigbanner 画报
     * @return 结果
     */
    public int updateSysBigbanner(SysBigbanner sysBigbanner);

    /**
     * 删除画报
     * 
     * @param bannerId 画报ID
     * @return 结果
     */
    public int deleteSysBigbannerById(Long bannerId);

    /**
     * 批量删除画报
     * 
     * @param bannerIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysBigbannerByIds(Long[] bannerIds);
    public int countShowBanner();
}
