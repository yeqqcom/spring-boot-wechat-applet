package com.ruoyi.order.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单对象 sys_order
 * 
 * @author yhh
 * @date 2020-12-23
 */
public class SysOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private Long orderId;

    /** 收货人 */
    @Excel(name = "收货人")
    private String cusName;

    /** 备注 */
    @Excel(name = "备注")
    private String cusMessage;

    /** 下单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "下单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderTime;

    /** 取餐方式 */
    @Excel(name = "取餐方式")
    private String wayTake;

    /** 状态 */
    @Excel(name = "状态")
    private String orderState;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String payWay;

    /** 总费用 */
    @Excel(name = "总费用")
    private Long goodsTotalfee;

    /** 优惠费用 */
    @Excel(name = "优惠费用")
    private Long discountFee;

    /** 打包费用 */
    @Excel(name = "打包费用")
    private Long packFee;

    /** 实际费用 */
    @Excel(name = "实际费用")
    private Long actualFee;

    /** 商品总数 */
    @Excel(name = "商品总数")
    private Long goodsTotalnum;

    /** 下单门店 */
    @Excel(name = "下单门店")
    private String orderStore;

    /** 取餐号 */
    @Excel(name = "取餐号")
    private String orderTakenum;

    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setCusName(String cusName) 
    {
        this.cusName = cusName;
    }

    public String getCusName() 
    {
        return cusName;
    }
    public void setCusMessage(String cusMessage) 
    {
        this.cusMessage = cusMessage;
    }

    public String getCusMessage() 
    {
        return cusMessage;
    }
    public void setOrderTime(Date orderTime) 
    {
        this.orderTime = orderTime;
    }

    public Date getOrderTime() 
    {
        return orderTime;
    }
    public void setWayTake(String wayTake) 
    {
        this.wayTake = wayTake;
    }

    public String getWayTake() 
    {
        return wayTake;
    }
    public void setOrderState(String orderState) 
    {
        this.orderState = orderState;
    }

    public String getOrderState() 
    {
        return orderState;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setPayWay(String payWay) 
    {
        this.payWay = payWay;
    }

    public String getPayWay() 
    {
        return payWay;
    }
    public void setGoodsTotalfee(Long goodsTotalfee) 
    {
        this.goodsTotalfee = goodsTotalfee;
    }

    public Long getGoodsTotalfee() 
    {
        return goodsTotalfee;
    }
    public void setDiscountFee(Long discountFee) 
    {
        this.discountFee = discountFee;
    }

    public Long getDiscountFee() 
    {
        return discountFee;
    }
    public void setPackFee(Long packFee) 
    {
        this.packFee = packFee;
    }

    public Long getPackFee() 
    {
        return packFee;
    }
    public void setActualFee(Long actualFee) 
    {
        this.actualFee = actualFee;
    }

    public Long getActualFee() 
    {
        return actualFee;
    }
    public void setGoodsTotalnum(Long goodsTotalnum) 
    {
        this.goodsTotalnum = goodsTotalnum;
    }

    public Long getGoodsTotalnum() 
    {
        return goodsTotalnum;
    }
    public void setOrderStore(String orderStore) 
    {
        this.orderStore = orderStore;
    }

    public String getOrderStore() 
    {
        return orderStore;
    }
    public void setOrderTakenum(String orderTakenum) 
    {
        this.orderTakenum = orderTakenum;
    }

    public String getOrderTakenum() 
    {
        return orderTakenum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("orderId", getOrderId())
            .append("cusName", getCusName())
            .append("cusMessage", getCusMessage())
            .append("orderTime", getOrderTime())
            .append("wayTake", getWayTake())
            .append("orderState", getOrderState())
            .append("address", getAddress())
            .append("payWay", getPayWay())
            .append("goodsTotalfee", getGoodsTotalfee())
            .append("discountFee", getDiscountFee())
            .append("packFee", getPackFee())
            .append("actualFee", getActualFee())
            .append("goodsTotalnum", getGoodsTotalnum())
            .append("orderStore", getOrderStore())
            .append("orderTakenum", getOrderTakenum())
            .toString();
    }
}
