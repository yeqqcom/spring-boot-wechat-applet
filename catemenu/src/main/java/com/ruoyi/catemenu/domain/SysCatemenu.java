package com.ruoyi.catemenu.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 分级菜单对象 sys_catemenu
 * 
 * @author yhh
 * @date 2020-12-23
 */
public class SysCatemenu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 菜单编号 */
    @Excel(name = "菜单编号")
    private Long menuId;

    /** 分级名称 */
    @Excel(name = "分级名称")
    private String menuName;

    /** 菜单优先级 */
    @Excel(name = "菜单优先级")
    private Long menuSort;

    public void setMenuId(Long menuId) 
    {
        this.menuId = menuId;
    }

    public Long getMenuId() 
    {
        return menuId;
    }
    public void setMenuName(String menuName) 
    {
        this.menuName = menuName;
    }

    public String getMenuName() 
    {
        return menuName;
    }
    public void setMenuSort(Long menuSort) 
    {
        this.menuSort = menuSort;
    }

    public Long getMenuSort() 
    {
        return menuSort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("menuId", getMenuId())
            .append("menuName", getMenuName())
            .append("menuSort", getMenuSort())
            .toString();
    }
}
