package com.ruoyi.catemenu.mapper;

import java.util.List;
import com.ruoyi.catemenu.domain.SysCatemenu;

/**
 * 分级菜单Mapper接口
 * 
 * @author yhh
 * @date 2020-12-23
 */
public interface SysCatemenuMapper 
{
    /**
     * 查询分级菜单
     * 
     * @param menuId 分级菜单ID
     * @return 分级菜单
     */
    public SysCatemenu selectSysCatemenuById(Long menuId);

    /**
     * 查询分级菜单列表
     * 
     * @param sysCatemenu 分级菜单
     * @return 分级菜单集合
     */
    public List<SysCatemenu> selectSysCatemenuList(SysCatemenu sysCatemenu);

    /**
     * 返回分级菜单按优先级排序后的列表
     *
     *
     * @return 分级菜单集合
     */
    public List<SysCatemenu> getSysCatemenuListbySort();
    /**
     * 新增分级菜单
     * 
     * @param sysCatemenu 分级菜单
     * @return 结果
     */
    public int insertSysCatemenu(SysCatemenu sysCatemenu);

    /**
     * 修改分级菜单
     * 
     * @param sysCatemenu 分级菜单
     * @return 结果
     */
    public int updateSysCatemenu(SysCatemenu sysCatemenu);

    /**
     * 删除分级菜单
     * 
     * @param menuId 分级菜单ID
     * @return 结果
     */
    public int deleteSysCatemenuById(Long menuId);

    /**
     * 批量删除分级菜单
     *
     * @param menuIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysCatemenuByIds(Long[] menuIds);
    /**
     * 获取分级菜单的数量
     *
     *
     * @return 结果
     */
    public int countMenu();

}
