package com.ruoyi.catemenu.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.catemenu.mapper.SysCatemenuMapper;
import com.ruoyi.catemenu.domain.SysCatemenu;
import com.ruoyi.catemenu.service.ISysCatemenuService;

/**
 * 分级菜单Service业务层处理
 * 
 * @author yhh
 * @date 2020-12-23
 */
@Service
public class SysCatemenuServiceImpl implements ISysCatemenuService 
{
    @Autowired
    private SysCatemenuMapper sysCatemenuMapper;

    /**
     * 查询分级菜单
     * 
     * @param menuId 分级菜单ID
     * @return 分级菜单
     */
    @Override
    public SysCatemenu selectSysCatemenuById(Long menuId)
    {
        return sysCatemenuMapper.selectSysCatemenuById(menuId);
    }

    /**
     * 查询分级菜单列表
     * 
     * @param sysCatemenu 分级菜单
     * @return 分级菜单
     */
    @Override
    public List<SysCatemenu> selectSysCatemenuList(SysCatemenu sysCatemenu)
    {
        return sysCatemenuMapper.selectSysCatemenuList(sysCatemenu);
    }


    /**
     * 按菜单优先级返回菜单序列
     *
     *
     * @return 分级菜单
     */
    @Override
    public List<SysCatemenu> getSysCatemenuListbySort(){
        return sysCatemenuMapper.getSysCatemenuListbySort();
    }

    @Override
    public int countMenu() {
        return sysCatemenuMapper.countMenu();
    }

    /**
     * 新增分级菜单
     * 
     * @param sysCatemenu 分级菜单
     * @return 结果
     */
    @Override
    public int insertSysCatemenu(SysCatemenu sysCatemenu)
    {
        return sysCatemenuMapper.insertSysCatemenu(sysCatemenu);
    }

    /**
     * 修改分级菜单
     * 
     * @param sysCatemenu 分级菜单
     * @return 结果
     */
    @Override
    public int updateSysCatemenu(SysCatemenu sysCatemenu)
    {
        return sysCatemenuMapper.updateSysCatemenu(sysCatemenu);
    }

    /**
     * 批量删除分级菜单
     * 
     * @param menuIds 需要删除的分级菜单ID
     * @return 结果
     */
    @Override
    public int deleteSysCatemenuByIds(Long[] menuIds)
    {
        return sysCatemenuMapper.deleteSysCatemenuByIds(menuIds);
    }

    /**
     * 删除分级菜单信息
     * 
     * @param menuId 分级菜单ID
     * @return 结果
     */
    @Override
    public int deleteSysCatemenuById(Long menuId)
    {
        return sysCatemenuMapper.deleteSysCatemenuById(menuId);
    }
}
