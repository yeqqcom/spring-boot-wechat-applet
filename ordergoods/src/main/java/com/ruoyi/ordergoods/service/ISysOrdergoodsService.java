package com.ruoyi.ordergoods.service;

import java.util.List;
import com.ruoyi.ordergoods.domain.SysOrdergoods;

/**
 * 订单内商品Service接口
 * 
 * @author yhh
 * @date 2020-12-23
 */
public interface ISysOrdergoodsService 
{
    /**
     * 查询订单内商品
     * 
     * @param orderId 订单内商品ID
     * @return 订单内商品
     */
    public SysOrdergoods selectSysOrdergoodsById(Long orderId);

    /**
     * 查询订单内商品列表
     * 
     * @param sysOrdergoods 订单内商品
     * @return 订单内商品集合
     */
    public List<SysOrdergoods> selectSysOrdergoodsList(SysOrdergoods sysOrdergoods);

    /**
     * 新增订单内商品
     * 
     * @param sysOrdergoods 订单内商品
     * @return 结果
     */
    public int insertSysOrdergoods(SysOrdergoods sysOrdergoods);

    /**
     * 修改订单内商品
     * 
     * @param sysOrdergoods 订单内商品
     * @return 结果
     */
    public int updateSysOrdergoods(SysOrdergoods sysOrdergoods);

    /**
     * 批量删除订单内商品
     * 
     * @param orderIds 需要删除的订单内商品ID
     * @return 结果
     */
    public int deleteSysOrdergoodsByIds(Long[] orderIds);

    /**
     * 删除订单内商品信息
     * 
     * @param orderId 订单内商品ID
     * @return 结果
     */
    public int deleteSysOrdergoodsById(Long orderId);
}
