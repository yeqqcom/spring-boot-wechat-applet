package com.ruoyi.ordergoods.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ordergoods.mapper.SysOrdergoodsMapper;
import com.ruoyi.ordergoods.domain.SysOrdergoods;
import com.ruoyi.ordergoods.service.ISysOrdergoodsService;

/**
 * 订单内商品Service业务层处理
 * 
 * @author yhh
 * @date 2020-12-23
 */
@Service
public class SysOrdergoodsServiceImpl implements ISysOrdergoodsService 
{
    @Autowired
    private SysOrdergoodsMapper sysOrdergoodsMapper;

    /**
     * 查询订单内商品
     * 
     * @param orderId 订单内商品ID
     * @return 订单内商品
     */
    @Override
    public SysOrdergoods selectSysOrdergoodsById(Long orderId)
    {
        return sysOrdergoodsMapper.selectSysOrdergoodsById(orderId);
    }

    /**
     * 查询订单内商品列表
     * 
     * @param sysOrdergoods 订单内商品
     * @return 订单内商品
     */
    @Override
    public List<SysOrdergoods> selectSysOrdergoodsList(SysOrdergoods sysOrdergoods)
    {
        return sysOrdergoodsMapper.selectSysOrdergoodsList(sysOrdergoods);
    }

    /**
     * 新增订单内商品
     * 
     * @param sysOrdergoods 订单内商品
     * @return 结果
     */
    @Override
    public int insertSysOrdergoods(SysOrdergoods sysOrdergoods)
    {
        return sysOrdergoodsMapper.insertSysOrdergoods(sysOrdergoods);
    }

    /**
     * 修改订单内商品
     * 
     * @param sysOrdergoods 订单内商品
     * @return 结果
     */
    @Override
    public int updateSysOrdergoods(SysOrdergoods sysOrdergoods)
    {
        return sysOrdergoodsMapper.updateSysOrdergoods(sysOrdergoods);
    }

    /**
     * 批量删除订单内商品
     * 
     * @param orderIds 需要删除的订单内商品ID
     * @return 结果
     */
    @Override
    public int deleteSysOrdergoodsByIds(Long[] orderIds)
    {
        return sysOrdergoodsMapper.deleteSysOrdergoodsByIds(orderIds);
    }

    /**
     * 删除订单内商品信息
     * 
     * @param orderId 订单内商品ID
     * @return 结果
     */
    @Override
    public int deleteSysOrdergoodsById(Long orderId)
    {
        return sysOrdergoodsMapper.deleteSysOrdergoodsById(orderId);
    }
}
