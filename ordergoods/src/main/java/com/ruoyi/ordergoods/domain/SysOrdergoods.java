package com.ruoyi.ordergoods.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单内商品对象 sys_ordergoods
 * 
 * @author yhh
 * @date 2020-12-23
 */
public class SysOrdergoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private Long orderId;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private Long goodsId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodsName;

    /** 规格值 */
    @Excel(name = "规格值")
    private String stanNum;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Integer goodsNum;

    /** 单个商品总价 */
    @Excel(name = "单个商品总价")
    private Long goodsAllfee;

    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setStanNum(String stanNum) 
    {
        this.stanNum = stanNum;
    }

    public String getStanNum() 
    {
        return stanNum;
    }
    public void setGoodsNum(Integer goodsNum) 
    {
        this.goodsNum = goodsNum;
    }

    public Integer getGoodsNum() 
    {
        return goodsNum;
    }
    public void setGoodsAllfee(Long goodsAllfee) 
    {
        this.goodsAllfee = goodsAllfee;
    }

    public Long getGoodsAllfee() 
    {
        return goodsAllfee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("orderId", getOrderId())
            .append("goodsId", getGoodsId())
            .append("goodsName", getGoodsName())
            .append("stanNum", getStanNum())
            .append("goodsNum", getGoodsNum())
            .append("goodsAllfee", getGoodsAllfee())
            .toString();
    }
}
