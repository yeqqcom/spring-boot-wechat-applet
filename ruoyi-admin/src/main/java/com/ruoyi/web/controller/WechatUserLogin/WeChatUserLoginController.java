package com.ruoyi.web.controller.WechatUserLogin;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.getopenid.service.WeChatUserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeChatUserLoginController {
    @Autowired
    WeChatUserLoginService wc;
    @RequestMapping("/getopenid")
    public String getcode(@RequestParam(value = "code")String  code){
        System.out.println(code);
        return wc.codetoopenid(code);
    }
}
