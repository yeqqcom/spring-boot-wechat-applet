package com.ruoyi.web.controller.catemenu;

import com.ruoyi.catemenu.domain.SysCatemenu;
import com.ruoyi.catemenu.service.ISysCatemenuService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 分级菜单Controller
 * 
 * @author yhh
 * @date 2020-12-23
 */
@RestController
@RequestMapping("/catemenu/show_catemenu")
public class SysCatemenuController extends BaseController
{
    @Autowired
    private ISysCatemenuService sysCatemenuService;

    /**
     * 查询分级菜单列表
     */
    @PreAuthorize("@ss.hasPermi('catemenu:show_catemenu:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysCatemenu sysCatemenu)
    {
        startPage();
        List<SysCatemenu> list = sysCatemenuService.selectSysCatemenuList(sysCatemenu);
        return getDataTable(list);
    }

    /**
     * 导出分级菜单列表
     */
    @PreAuthorize("@ss.hasPermi('catemenu:show_catemenu:export')")
    @Log(title = "分级菜单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysCatemenu sysCatemenu)
    {
        List<SysCatemenu> list = sysCatemenuService.selectSysCatemenuList(sysCatemenu);
        ExcelUtil<SysCatemenu> util = new ExcelUtil<SysCatemenu>(SysCatemenu.class);
        return util.exportExcel(list, "show_catemenu");
    }

    /**
     * 获取分级菜单详细信息
     */
    @PreAuthorize("@ss.hasPermi('catemenu:show_catemenu:query')")
    @GetMapping(value = "/{menuId}")
    public AjaxResult getInfo(@PathVariable("menuId") Long menuId)
    {
        return AjaxResult.success(sysCatemenuService.selectSysCatemenuById(menuId));
    }

    /**
     * 按分级菜单次序获取菜单表
     */
//    @PreAuthorize("@ss.hasPermi('catemenu:show_catemenu:list')")
    @GetMapping("/sortlist")
    public List<SysCatemenu> sortlist()
    {

        List<SysCatemenu> list = sysCatemenuService.getSysCatemenuListbySort();
        return list;
    }

    /**
     * 新增分级菜单
     */
    @PreAuthorize("@ss.hasPermi('catemenu:show_catemenu:add')")
    @Log(title = "分级菜单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysCatemenu sysCatemenu)
    {
        return toAjax(sysCatemenuService.insertSysCatemenu(sysCatemenu));
    }

    /**
     * 修改分级菜单
     */
    @PreAuthorize("@ss.hasPermi('catemenu:show_catemenu:edit')")
    @Log(title = "分级菜单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysCatemenu sysCatemenu)
    {
        return toAjax(sysCatemenuService.updateSysCatemenu(sysCatemenu));
    }

    /**
     * 删除分级菜单
     */
    @PreAuthorize("@ss.hasPermi('catemenu:show_catemenu:remove')")
    @Log(title = "分级菜单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{menuIds}")
    public AjaxResult remove(@PathVariable Long[] menuIds)
    {
        return toAjax(sysCatemenuService.deleteSysCatemenuByIds(menuIds));
    }

    @GetMapping("/menucount")
    public int MenuCount()
    {
        int menucount = sysCatemenuService.countMenu();
        return menucount;
    }


}
