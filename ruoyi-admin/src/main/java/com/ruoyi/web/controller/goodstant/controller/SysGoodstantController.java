package com.ruoyi.web.controller.goodstant.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.goodstant.domain.SysGoodstant;
import com.ruoyi.goodstant.service.ISysGoodstantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商品规格Controller
 * 
 * @author yhh
 * @date 2020-12-22
 */
@RestController
@RequestMapping("/goodstant/show_goodstant")
public class SysGoodstantController extends BaseController
{
    @Autowired
    private ISysGoodstantService sysGoodstantService;

    /**
     * 查询商品规格列表
     */
    @PreAuthorize("@ss.hasPermi('goodstant:show_goodstant:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysGoodstant sysGoodstant)
    {
        startPage();
        List<SysGoodstant> list = sysGoodstantService.selectSysGoodstantList(sysGoodstant);
        return getDataTable(list);
    }

    /**
     * 导出商品规格列表
     */
    @PreAuthorize("@ss.hasPermi('goodstant:show_goodstant:export')")
    @Log(title = "商品规格", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysGoodstant sysGoodstant)
    {
        List<SysGoodstant> list = sysGoodstantService.selectSysGoodstantList(sysGoodstant);
        ExcelUtil<SysGoodstant> util = new ExcelUtil<SysGoodstant>(SysGoodstant.class);
        return util.exportExcel(list, "show_goodstant");
    }

    /**
     * 获取商品规格详细信息
     */
    @PreAuthorize("@ss.hasPermi('goodstant:show_goodstant:query')")
    @GetMapping(value = "/{stanId}")
    public AjaxResult getInfo(@PathVariable("stanId") Long stanId)
    {
        return AjaxResult.success(sysGoodstantService.selectSysGoodstantById(stanId));
    }

    /**
     * 新增商品规格
     */
    @PreAuthorize("@ss.hasPermi('goodstant:show_goodstant:add')")
    @Log(title = "商品规格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysGoodstant sysGoodstant)
    {
        return toAjax(sysGoodstantService.insertSysGoodstant(sysGoodstant));
    }

    /**
     * 修改商品规格
     */
    @PreAuthorize("@ss.hasPermi('goodstant:show_goodstant:edit')")
    @Log(title = "商品规格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysGoodstant sysGoodstant)
    {
        return toAjax(sysGoodstantService.updateSysGoodstant(sysGoodstant));
    }

    /**
     * 删除商品规格
     */
    @PreAuthorize("@ss.hasPermi('goodstant:show_goodstant:remove')")
    @Log(title = "商品规格", businessType = BusinessType.DELETE)
	@DeleteMapping("/{stanIds}")
    public AjaxResult remove(@PathVariable Long[] stanIds)
    {
        return toAjax(sysGoodstantService.deleteSysGoodstantByIds(stanIds));
    }
}
