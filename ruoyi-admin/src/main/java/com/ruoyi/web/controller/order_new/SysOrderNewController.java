package com.ruoyi.web.controller.order_new;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.customers.service.ISysCustomersService;
import com.ruoyi.order_new.domain.OrderStatistical;
import com.ruoyi.order_new.domain.SysOrderNew;
import com.ruoyi.order_new.domain.dayStatistical;
import com.ruoyi.order_new.service.ISysOrderNewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 总订单Controller
 * 
 * @author yeziyin
 * @date 2021-01-02
 */
@RestController
@RequestMapping("/order_new/show_order_new")
public class SysOrderNewController extends BaseController
{
    @Autowired
    private ISysOrderNewService sysOrderNewService;

    @Autowired
    private ISysCustomersService sysCustomersService;

    /**
     * 查询总订单列表
     */
    @PreAuthorize("@ss.hasPermi('order_new:show_order_new:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysOrderNew sysOrderNew)
    {
        startPage();
        List<SysOrderNew> list = sysOrderNewService.selectSysOrderNewList(sysOrderNew);
        return getDataTable(list);
    }

    /**
     * 导出总订单列表
     */
    @PreAuthorize("@ss.hasPermi('order_new:show_order_new:export')")
    @Log(title = "总订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysOrderNew sysOrderNew)
    {
        List<SysOrderNew> list = sysOrderNewService.selectSysOrderNewList(sysOrderNew);
        ExcelUtil<SysOrderNew> util = new ExcelUtil<SysOrderNew>(SysOrderNew.class);
        return util.exportExcel(list, "show_order_new");
    }

    /**
     * 获取总订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('order_new:show_order_new:query')")
    @GetMapping(value = "/{orderid}")
    public AjaxResult getInfo(@PathVariable("orderid") Long orderid)
    {
        return AjaxResult.success(sysOrderNewService.selectSysOrderNewById(orderid));
    }

    /**
     * 新增总订单
     */
    @PreAuthorize("@ss.hasPermi('order_new:show_order_new:add')")
    @Log(title = "总订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysOrderNew sysOrderNew)
    {

        BigDecimal price = sysOrderNew.getOrderallprice();
        long point = price.longValue();
        String openid = sysOrderNew.getUseropenid();
        sysCustomersService.updateCustomerLever(openid, point);
        return toAjax(sysOrderNewService.insertSysOrderNew(sysOrderNew));
    }

    /**
     * 修改总订单
     */
    @PreAuthorize("@ss.hasPermi('order_new:show_order_new:edit')")
    @Log(title = "总订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysOrderNew sysOrderNew)
    {

        return toAjax(sysOrderNewService.updateSysOrderNew(sysOrderNew));
    }

    /**
     * 删除总订单
     */
    @PreAuthorize("@ss.hasPermi('order_new:show_order_new:remove')")
    @Log(title = "总订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{orderids}")
    public AjaxResult remove(@PathVariable Long[] orderids)
    {
        return toAjax(sysOrderNewService.deleteSysOrderNewByIds(orderids));
    }

    /**获取订单总体情况统计**/
    @GetMapping("/getorderstatistical")
    public List<OrderStatistical> getorderstatistical(){
        return sysOrderNewService.getOrderStatistical();
    }

    /**获取订单七天情况统计**/
    @GetMapping("/getweeklyorderstatistical")
    public List<dayStatistical> getWeeklyorderstatistical(){
        return sysOrderNewService.getWeeklyDayStatistical();
    }
}
