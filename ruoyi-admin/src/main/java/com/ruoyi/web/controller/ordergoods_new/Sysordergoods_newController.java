package com.ruoyi.web.controller.ordergoods_new;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.ordergoods_news.domain.Sysordergoods_new;
import com.ruoyi.ordergoods_news.domain.orderAllMassageList;
import com.ruoyi.ordergoods_news.service.ISysordergoods_newService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单详细商品Controller
 * 
 * @author yhh
 * @date 2020-12-25
 */
@RestController
@RequestMapping("/ordergoods_new/show_ordergoods_new")
public class Sysordergoods_newController extends BaseController
{
    @Autowired
    private ISysordergoods_newService sysordergoods_newService;

    /**
     * 查询订单详细商品列表
     */
    @PreAuthorize("@ss.hasPermi('ordergoods_new:show_ordergoods_new:list')")
    @GetMapping("/list")
    public TableDataInfo list(Sysordergoods_new sysordergoods_new)
    {
        startPage();
        List<Sysordergoods_new> list = sysordergoods_newService.selectSysordergoods_newList(sysordergoods_new);
        return getDataTable(list);
    }

    /**
     * 导出订单详细商品列表
     */
    @PreAuthorize("@ss.hasPermi('ordergoods_new:show_ordergoods_new:export')")
    @Log(title = "订单详细商品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Sysordergoods_new sysordergoods_new)
    {
        List<Sysordergoods_new> list = sysordergoods_newService.selectSysordergoods_newList(sysordergoods_new);
        ExcelUtil<Sysordergoods_new> util = new ExcelUtil<Sysordergoods_new>(Sysordergoods_new.class);
        return util.exportExcel(list, "show_ordergoods_new");
    }

    /**
     * 获取订单详细商品详细信息
     */
//    @PreAuthorize("@ss.hasPermi('ordergoods_new:show_ordergoods_new:query')")
//    @GetMapping(value = "/{username}")
//    public AjaxResult getInfo(@PathVariable("username") String username)
//    {
//        return AjaxResult.success(sysordergoods_newService.selectSysordergoods_newById(username));
//    }
    /**
     * 按登录用户（openid）获取当日订单大概信息
     */
    @GetMapping(value = "/gettodayorderbyopenid")
    public  List<orderAllMassageList> getTodayOrderListByOpenid(String useropenid)
    {
        return  sysordergoods_newService.getTodayOrderListByOpenid(useropenid);
    }
    /**
     * 按登录用户（openid）获取历史订单大概信息
     */
    @GetMapping(value = "/getallorderbyopenid")
    public  List<orderAllMassageList> getAllOrderListByOpenid(String useropenid)
    {
        return  sysordergoods_newService.getAllOrderListByOpenid(useropenid);
    }
    /**
     * 按订单号获取订单的详细信息
     */

    /**
     * 新增订单详细商品
     */
    @PreAuthorize("@ss.hasPermi('ordergoods_new:show_ordergoods_new:add')")
    @Log(title = "订单详细商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Sysordergoods_new sysordergoods_new)
    {
        return toAjax(sysordergoods_newService.insertSysordergoods_new(sysordergoods_new));
    }
    /**
     * 从小程序回传订单数据后的新增数据
     */
    @RequestMapping("/addOneOrderByStr")
    public String addOneOrder(String useropenid,String userName,String goodsStr,String Phone)
    {

        return sysordergoods_newService.addOneOrderByStr(useropenid, userName, goodsStr,Phone);
    }
    /**
     * 修改订单详细商品
     */
    @PreAuthorize("@ss.hasPermi('ordergoods_new:show_ordergoods_new:edit')")
    @Log(title = "订单详细商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Sysordergoods_new sysordergoods_new)
    {
        return toAjax(sysordergoods_newService.updateSysordergoods_new(sysordergoods_new));
    }

    /**
     * 删除订单详细商品
     */
    @PreAuthorize("@ss.hasPermi('ordergoods_new:show_ordergoods_new:remove')")
    @Log(title = "订单详细商品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable int[] ids)
    {
        return toAjax(sysordergoods_newService.deleteSysordergoods_newByIds(ids));
    }
}
