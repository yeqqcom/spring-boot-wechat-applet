package com.ruoyi.web.controller.ordergoods;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.ordergoods.domain.SysOrdergoods;
import com.ruoyi.ordergoods.service.ISysOrdergoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单内商品Controller
 * 
 * @author yhh
 * @date 2020-12-23
 */
@RestController
@RequestMapping("/ordergoods/show_ordergoods")
public class SysOrdergoodsController extends BaseController
{
    @Autowired
    private ISysOrdergoodsService sysOrdergoodsService;

    /**
     * 查询订单内商品列表
     */
    @PreAuthorize("@ss.hasPermi('ordergoods:show_ordergoods:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysOrdergoods sysOrdergoods)
    {
        startPage();
        List<SysOrdergoods> list = sysOrdergoodsService.selectSysOrdergoodsList(sysOrdergoods);
        return getDataTable(list);
    }

    /**
     * 导出订单内商品列表
     */
    @PreAuthorize("@ss.hasPermi('ordergoods:show_ordergoods:export')")
    @Log(title = "订单内商品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysOrdergoods sysOrdergoods)
    {
        List<SysOrdergoods> list = sysOrdergoodsService.selectSysOrdergoodsList(sysOrdergoods);
        ExcelUtil<SysOrdergoods> util = new ExcelUtil<SysOrdergoods>(SysOrdergoods.class);
        return util.exportExcel(list, "show_ordergoods");
    }

    /**
     * 获取订单内商品详细信息
     */
    @PreAuthorize("@ss.hasPermi('ordergoods:show_ordergoods:query')")
    @GetMapping(value = "/{orderId}")
    public AjaxResult getInfo(@PathVariable("orderId") Long orderId)
    {
        return AjaxResult.success(sysOrdergoodsService.selectSysOrdergoodsById(orderId));
    }

    /**
     * 新增订单内商品
     */
    @PreAuthorize("@ss.hasPermi('ordergoods:show_ordergoods:add')")
    @Log(title = "订单内商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysOrdergoods sysOrdergoods)
    {
        return toAjax(sysOrdergoodsService.insertSysOrdergoods(sysOrdergoods));
    }

    /**
     * 修改订单内商品
     */
    @PreAuthorize("@ss.hasPermi('ordergoods:show_ordergoods:edit')")
    @Log(title = "订单内商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysOrdergoods sysOrdergoods)
    {
        return toAjax(sysOrdergoodsService.updateSysOrdergoods(sysOrdergoods));
    }

    /**
     * 删除订单内商品
     */
    @PreAuthorize("@ss.hasPermi('ordergoods:show_ordergoods:remove')")
    @Log(title = "订单内商品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{orderIds}")
    public AjaxResult remove(@PathVariable Long[] orderIds)
    {
        return toAjax(sysOrdergoodsService.deleteSysOrdergoodsByIds(orderIds));
    }
}
