package com.ruoyi.web.controller.goods_new;

import java.util.ArrayList;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.goods_new.domain.SysGoodsNew;
import com.ruoyi.goods_new.service.ISysGoodsNewService;
import com.ruoyi.catemenu.domain.SysCatemenu;
import com.ruoyi.catemenu.service.ISysCatemenuService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品Controller
 * 
 * @author yhh
 * @date 2020-12-25
 */
@RestController
@RequestMapping("/goods_new/show_goods_new")
public class SysGoodsNewController extends BaseController {
    @Autowired
    private ISysGoodsNewService sysGoodsNewService;
    @Autowired
    private ISysCatemenuService sysCatemenuService;

    /**
     * 查询商品列表
     */
    @PreAuthorize("@ss.hasPermi('goods_new:show_goods_new:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysGoodsNew sysGoodsNew) {
        startPage();
        List<SysGoodsNew> list = sysGoodsNewService.selectSysGoodsNewList(sysGoodsNew);
        return getDataTable(list);
    }

    /**
     * 导出商品列表
     */
   @PreAuthorize("@ss.hasPermi('goods_new:show_goods_new:export')")
    @Log(title = "商品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysGoodsNew sysGoodsNew) {
        List<SysGoodsNew> list = sysGoodsNewService.selectSysGoodsNewList(sysGoodsNew);
        ExcelUtil<SysGoodsNew> util = new ExcelUtil<SysGoodsNew>(SysGoodsNew.class);
        return util.exportExcel(list, "show_goods_new");
    }

    /**
     * 获取商品详细信息
     */
//    @PreAuthorize("@ss.hasPermi('goods_new:show_goods_new:query')")
    @GetMapping(value = "/{goodsId}")
    public AjaxResult getInfo(@PathVariable("goodsId") Long goodsId) {
        return AjaxResult.success(sysGoodsNewService.selectSysGoodsNewById(goodsId));
    }
    /**
     * 小程序获取商品详细信息
     */

    @GetMapping(value = "/getgoodsbyid/{goodsId}")
    public SysGoodsNew getDetil(@PathVariable("goodsId") Long goodsId) {
        return sysGoodsNewService.selectSysGoodsNewById(goodsId);
    }

    /**
     * 新增商品
     */
    @PreAuthorize("@ss.hasPermi('goods_new:show_goods_new:add')")
    @Log(title = "商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysGoodsNew sysGoodsNew) {
        return toAjax(sysGoodsNewService.insertSysGoodsNew(sysGoodsNew));
    }

    /**
     * 修改商品
     */
    @PreAuthorize("@ss.hasPermi('goods_new:show_goods_new:edit')")
    @Log(title = "商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysGoodsNew sysGoodsNew) {
        return toAjax(sysGoodsNewService.updateSysGoodsNew(sysGoodsNew));
    }

    /**
     * 删除商品
     */
    @PreAuthorize("@ss.hasPermi('goods_new:show_goods_new:remove')")
    @Log(title = "商品", businessType = BusinessType.DELETE)
    @DeleteMapping("/{goodsIds}")
    public AjaxResult remove(@PathVariable Long[] goodsIds) {
        return toAjax(sysGoodsNewService.deleteSysGoodsNewByIds(goodsIds));
    }

    @GetMapping("/selectbymenuid")
//    返回对应分类的奶茶
    public List<SysGoodsNew> selectSysGoodsByMenuid(Long Menuid) {
        List<SysGoodsNew> SysGoodsNew = sysGoodsNewService.selectSysGoodsByMenuid(Menuid);
        return SysGoodsNew;
    }
    @GetMapping("/goodsnuminOneMenu")
    public int goodsnuminOneMenu(Long Menuid){
        return sysGoodsNewService.goodsnuminOneMenu(Menuid);
    }

    @GetMapping("/selectAllGoodsByMenu")
    public List<List<SysGoodsNew>> selectAllinMenu() {
        List<List<SysGoodsNew>> res = new ArrayList<List<SysGoodsNew>>();
//        int menucount = sysCatemenuService.countMenu();
        List<SysCatemenu> sortmenu = sysCatemenuService.getSysCatemenuListbySort();
        System.out.println(sortmenu);
        for (SysCatemenu syscatmenu : sortmenu) {
            Long menuid = syscatmenu.getMenuId();
            if (goodsnuminOneMenu(menuid) > 0) {
                System.out.println(goodsnuminOneMenu(menuid));
                res.add(selectSysGoodsByMenuid(menuid));
            }
        }
        return res;
    }

}
