package com.ruoyi.web.controller.bigbanner;

import com.ruoyi.bigbanner.domain.SysBigbanner;
import com.ruoyi.bigbanner.service.ISysBigbannerService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 画报Controller
 * 
 * @author yhh
 * @date 2020-12-23
 */
@RestController
@RequestMapping("/bingbanner/show_bigbanner")
public class SysBigbannerController extends BaseController
{
    @Autowired
    private ISysBigbannerService sysBigbannerService;

    /**
     * 查询画报列表
     */
    @PreAuthorize("@ss.hasPermi('bingbanner:show_bigbanner:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysBigbanner sysBigbanner)
    {
        startPage();
        List<SysBigbanner> list = sysBigbannerService.selectSysBigbannerList(sysBigbanner);
        return getDataTable(list);
    }

    /**
     * 导出画报列表
     */
    @PreAuthorize("@ss.hasPermi('bingbanner:show_bigbanner:export')")
    @Log(title = "画报", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysBigbanner sysBigbanner)
    {
        List<SysBigbanner> list = sysBigbannerService.selectSysBigbannerList(sysBigbanner);
        ExcelUtil<SysBigbanner> util = new ExcelUtil<SysBigbanner>(SysBigbanner.class);
        return util.exportExcel(list, "show_bigbanner");
    }

    /**
     * 获取画报详细信息
     */
    @PreAuthorize("@ss.hasPermi('bingbanner:show_bigbanner:query')")
    @GetMapping(value = "/{bannerId}")
    public AjaxResult getInfo(@PathVariable("bannerId") Long bannerId)
    {
        return AjaxResult.success(sysBigbannerService.selectSysBigbannerById(bannerId));
    }
    /**
     * 根据状态获取画报详细地址
     */
//    @PreAuthorize("@ss.hasPermi('bingbanner:show_bigbanner:query')")
    @GetMapping(value = "/getbystate/{State}")
    public AjaxResult getInfo(@PathVariable("State") String State)
    {
        return AjaxResult.success(sysBigbannerService.selectSysBigbannerByState(State));
    }

    /**
     * 新增画报
     */
    @PreAuthorize("@ss.hasPermi('bingbanner:show_bigbanner:add')")
    @Log(title = "画报", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysBigbanner sysBigbanner)
    {
        return toAjax(sysBigbannerService.insertSysBigbanner(sysBigbanner));
    }

    /**
     * 修改画报
     */
    @PreAuthorize("@ss.hasPermi('bingbanner:show_bigbanner:edit')")
    @Log(title = "画报", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysBigbanner sysBigbanner)
    {
        return toAjax(sysBigbannerService.updateSysBigbanner(sysBigbanner));
    }

    /**
     * 删除画报
     */
    @PreAuthorize("@ss.hasPermi('bingbanner:show_bigbanner:remove')")
    @Log(title = "画报", businessType = BusinessType.DELETE)
	@DeleteMapping("/{bannerIds}")
    public AjaxResult remove(@PathVariable Long[] bannerIds)
    {
        return toAjax(sysBigbannerService.deleteSysBigbannerByIds(bannerIds));
    }
}
