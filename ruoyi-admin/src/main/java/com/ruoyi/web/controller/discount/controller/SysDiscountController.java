package com.ruoyi.web.controller.discount.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.discount.domain.SysDiscount;
import com.ruoyi.discount.service.ISysDiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 优惠券Controller
 * 
 * @author yhh
 * @date 2020-12-22
 */
@RestController
@RequestMapping("/discount/show_discount")
public class SysDiscountController extends BaseController
{
    @Autowired
    private ISysDiscountService sysDiscountService;

    /**
     * 查询优惠券列表
     */
    @PreAuthorize("@ss.hasPermi('discount:show_discount:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysDiscount sysDiscount)
    {
        startPage();
        List<SysDiscount> list = sysDiscountService.selectSysDiscountList(sysDiscount);
        return getDataTable(list);
    }

    /**
     * 导出优惠券列表
     */
    @PreAuthorize("@ss.hasPermi('discount:show_discount:export')")
    @Log(title = "优惠券", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysDiscount sysDiscount)
    {
        List<SysDiscount> list = sysDiscountService.selectSysDiscountList(sysDiscount);
        ExcelUtil<SysDiscount> util = new ExcelUtil<SysDiscount>(SysDiscount.class);
        return util.exportExcel(list, "show_discount");
    }

    /**
     * 获取优惠券详细信息
     */
    @PreAuthorize("@ss.hasPermi('discount:show_discount:query')")
    @GetMapping(value = "/{discountId}")
    public AjaxResult getInfo(@PathVariable("discountId") Long discountId)
    {
        return AjaxResult.success(sysDiscountService.selectSysDiscountById(discountId));
    }

    /**
     * 新增优惠券
     */
    @PreAuthorize("@ss.hasPermi('discount:show_discount:add')")
    @Log(title = "优惠券", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysDiscount sysDiscount)
    {
        return toAjax(sysDiscountService.insertSysDiscount(sysDiscount));
    }

    /**
     * 修改优惠券
     */
    @PreAuthorize("@ss.hasPermi('discount:show_discount:edit')")
    @Log(title = "优惠券", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysDiscount sysDiscount)
    {
        return toAjax(sysDiscountService.updateSysDiscount(sysDiscount));
    }

    /**
     * 删除优惠券
     */
    @PreAuthorize("@ss.hasPermi('discount:show_discount:remove')")
    @Log(title = "优惠券", businessType = BusinessType.DELETE)
	@DeleteMapping("/{discountIds}")
    public AjaxResult remove(@PathVariable Long[] discountIds)
    {
        return toAjax(sysDiscountService.deleteSysDiscountByIds(discountIds));
    }
}
