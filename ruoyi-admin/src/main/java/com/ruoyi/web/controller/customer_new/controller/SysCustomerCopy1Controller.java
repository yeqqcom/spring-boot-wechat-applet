package com.ruoyi.web.controller.customer_new.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.ordergoods.domain.SysCustomerCopy1;
import com.ruoyi.ordergoods.service.ISysCustomerCopy1Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 客户个人信息Controller
 * 
 * @author yhh
 * @date 2020-12-25
 */
@RestController
@RequestMapping("/ordergoods/ordergoods_show")
public class SysCustomerCopy1Controller extends BaseController
{
    @Autowired
    private ISysCustomerCopy1Service sysCustomerCopy1Service;

    /**
     * 查询客户个人信息列表
     */
    @PreAuthorize("@ss.hasPermi('ordergoods:ordergoods_show:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysCustomerCopy1 sysCustomerCopy1)
    {
        startPage();
        List<SysCustomerCopy1> list = sysCustomerCopy1Service.selectSysCustomerCopy1List(sysCustomerCopy1);
        return getDataTable(list);
    }

    /**
     * 导出客户个人信息列表
     */
    @PreAuthorize("@ss.hasPermi('ordergoods:ordergoods_show:export')")
    @Log(title = "客户个人信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysCustomerCopy1 sysCustomerCopy1)
    {
        List<SysCustomerCopy1> list = sysCustomerCopy1Service.selectSysCustomerCopy1List(sysCustomerCopy1);
        ExcelUtil<SysCustomerCopy1> util = new ExcelUtil<SysCustomerCopy1>(SysCustomerCopy1.class);
        return util.exportExcel(list, "ordergoods_show");
    }

    /**
     * 获取客户个人信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('ordergoods:ordergoods_show:query')")
    @GetMapping(value = "/{cusId}")
    public AjaxResult getInfo(@PathVariable("cusId") Long cusId)
    {
        return AjaxResult.success(sysCustomerCopy1Service.selectSysCustomerCopy1ById(cusId));
    }

    /**
     * 新增客户个人信息
     */
    @PreAuthorize("@ss.hasPermi('ordergoods:ordergoods_show:add')")
    @Log(title = "客户个人信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysCustomerCopy1 sysCustomerCopy1)
    {
        return toAjax(sysCustomerCopy1Service.insertSysCustomerCopy1(sysCustomerCopy1));
    }

    /**
     * 修改客户个人信息
     */
    @PreAuthorize("@ss.hasPermi('ordergoods:ordergoods_show:edit')")
    @Log(title = "客户个人信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysCustomerCopy1 sysCustomerCopy1)
    {
        return toAjax(sysCustomerCopy1Service.updateSysCustomerCopy1(sysCustomerCopy1));
    }

    /**
     * 删除客户个人信息
     */
    @PreAuthorize("@ss.hasPermi('ordergoods:ordergoods_show:remove')")
    @Log(title = "客户个人信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cusIds}")
    public AjaxResult remove(@PathVariable Long[] cusIds)
    {
        return toAjax(sysCustomerCopy1Service.deleteSysCustomerCopy1ByIds(cusIds));
    }
}
