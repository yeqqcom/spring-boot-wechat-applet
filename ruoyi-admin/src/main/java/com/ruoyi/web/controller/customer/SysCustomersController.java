package com.ruoyi.web.controller.customer;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.customers.domain.SysCustomers;
import com.ruoyi.customers.service.ISysCustomersService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.xml.transform.Result;
import java.util.List;

/**
 * 客户个人信息Controller
 * 
 * @author yhh
 * @date 2020-12-23
 */
@RestController
@RequestMapping("/customers/show_customers")
public class SysCustomersController extends BaseController
{
    @Autowired
    private ISysCustomersService sysCustomersService;

    /**
     * 查询客户个人信息列表
     */
    @PreAuthorize("@ss.hasPermi('customers:show_customers:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysCustomers sysCustomers)
    {
        startPage();
        List<SysCustomers> list = sysCustomersService.selectSysCustomersList(sysCustomers);
        return getDataTable(list);
    }

    /**
     * 导出客户个人信息列表
     */
    @PreAuthorize("@ss.hasPermi('customers:show_customers:export')")
    @Log(title = "客户个人信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysCustomers sysCustomers)
    {
        List<SysCustomers> list = sysCustomersService.selectSysCustomersList(sysCustomers);
        ExcelUtil<SysCustomers> util = new ExcelUtil<SysCustomers>(SysCustomers.class);
        return util.exportExcel(list, "show_customers");
    }

    /**
     * 获取客户个人信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('customers:show_customers:query')")
    @GetMapping(value = "/{cusId}")
    public AjaxResult getInfo(@PathVariable("cusId") Long cusId)
    {
        return AjaxResult.success(sysCustomersService.selectSysCustomersById(cusId));
    }

    /**
     * 新增客户个人信息
     */
    @PreAuthorize("@ss.hasPermi('customers:show_customers:add')")
    @Log(title = "客户个人信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysCustomers sysCustomers)
    {
        return toAjax(sysCustomersService.insertSysCustomers(sysCustomers));
    }

    /**
     * 修改客户个人信息
     */
    @PreAuthorize("@ss.hasPermi('customers:show_customers:edit')")
    @Log(title = "客户个人信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysCustomers sysCustomers)
    {
        return toAjax(sysCustomersService.updateSysCustomers(sysCustomers));
    }

    /**
     * 删除客户个人信息
     */
    @PreAuthorize("@ss.hasPermi('customers:show_customers:remove')")
    @Log(title = "客户个人信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cusIds}")
    public AjaxResult remove(@PathVariable Long[] cusIds)
    {
        return toAjax(sysCustomersService.deleteSysCustomersByIds(cusIds));
    }
    /**
     * 根据userinfo查询客户个人信息,如果新用户则添加
     */
    @RequestMapping("/getcustomerInfoAndadd")
    public SysCustomers getcustomerInfoAndadd(String openid,String cusName,String cusImg,int cusSex)
    {
        String sex;
        if(cusSex == 1 ){ sex = "男"; }
        else { sex ="女"; }
        return sysCustomersService.getcustomerInfo(openid,cusName,cusImg,sex);
    }
    @RequestMapping("/getcustomerInfoOnly")
    public SysCustomers getcustomerInfoOnly(String openid)
    {
        return sysCustomersService.getcustomerInfobyopenid(openid);
    }

    @RequestMapping("/ChangeInfo")
    public Boolean changeCustomerInfo(String openid,String phone){
        return sysCustomersService.changeCustomerInfo(openid,phone);
    }



}
