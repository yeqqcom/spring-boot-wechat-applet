package com.ruoyi.web.controller.address;

import com.ruoyi.address.domain.SysAddress;
import com.ruoyi.address.service.ISysAddressService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户地址Controller
 * 
 * @author yhh
 * @date 2020-12-22
 */
@RestController
@RequestMapping("/address/show_address")
public class SysAddressController extends BaseController
{
    @Autowired
    private ISysAddressService sysAddressService;

    /**
     * 查询用户地址列表
     */
    @PreAuthorize("@ss.hasPermi('address:show_address:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysAddress sysAddress)
    {
        startPage();
        List<SysAddress> list = sysAddressService.selectSysAddressList(sysAddress);
        return getDataTable(list);
    }

    /**
     * 导出用户地址列表
     */
    @PreAuthorize("@ss.hasPermi('address:show_address:export')")
    @Log(title = "用户地址", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysAddress sysAddress)
    {
        List<SysAddress> list = sysAddressService.selectSysAddressList(sysAddress);
        ExcelUtil<SysAddress> util = new ExcelUtil<SysAddress>(SysAddress.class);
        return util.exportExcel(list, "show_address");
    }

    /**
     * 获取用户地址详细信息
     */
    @PreAuthorize("@ss.hasPermi('address:show_address:query')")
    @GetMapping(value = "/{cusId}")
    public AjaxResult getInfo(@PathVariable("cusId") Long cusId)
    {
        return AjaxResult.success(sysAddressService.selectSysAddressById(cusId));
    }

    /**
     * 新增用户地址
     */
    @PreAuthorize("@ss.hasPermi('address:show_address:add')")
    @Log(title = "用户地址", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysAddress sysAddress)
    {
        return toAjax(sysAddressService.insertSysAddress(sysAddress));
    }

    /**
     * 修改用户地址
     */
    @PreAuthorize("@ss.hasPermi('address:show_address:edit')")
    @Log(title = "用户地址", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysAddress sysAddress)
    {
        return toAjax(sysAddressService.updateSysAddress(sysAddress));
    }

    /**
     * 删除用户地址
     */
    @PreAuthorize("@ss.hasPermi('address:show_address:remove')")
    @Log(title = "用户地址", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cusIds}")
    public AjaxResult remove(@PathVariable Long[] cusIds)
    {
        return toAjax(sysAddressService.deleteSysAddressByIds(cusIds));
    }
}
