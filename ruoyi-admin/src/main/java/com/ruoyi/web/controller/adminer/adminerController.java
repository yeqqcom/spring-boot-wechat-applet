package com.ruoyi.web.controller.adminer;

import com.ruoyi.adminer.domain.adminer;
import com.ruoyi.adminer.service.IadminerService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 管理员Controller
 * 
 * @author yhh
 * @date 2020-12-22
 */
@RestController
@RequestMapping("/adminer/show_adminer")
public class adminerController extends BaseController
{
    @Autowired
    private IadminerService adminerService;

    /**
     * 查询管理员列表
     */
    @PreAuthorize("@ss.hasPermi('adminer:show_adminer:list')")
    @GetMapping("/list")
    public TableDataInfo list(adminer adminer)
    {
        startPage();
        List<adminer> list = adminerService.selectadminerList(adminer);
        return getDataTable(list);
    }

    /**
     * 导出管理员列表
     */
    @PreAuthorize("@ss.hasPermi('adminer:show_adminer:export')")
    @Log(title = "管理员", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(adminer adminer)
    {
        List<adminer> list = adminerService.selectadminerList(adminer);
        ExcelUtil<adminer> util = new ExcelUtil<adminer>(adminer.class);
        return util.exportExcel(list, "show_adminer");
    }

    /**
     * 获取管理员详细信息
     */
    @PreAuthorize("@ss.hasPermi('adminer:show_adminer:query')")
    @GetMapping(value = "/{adId}")
    public AjaxResult getInfo(@PathVariable("adId") Long adId)
    {
        return AjaxResult.success(adminerService.selectadminerById(adId));
    }

    /**
     * 新增管理员
     */
    @PreAuthorize("@ss.hasPermi('adminer:show_adminer:add')")
    @Log(title = "管理员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody adminer adminer)
    {
        return toAjax(adminerService.insertadminer(adminer));
    }

    /**
     * 修改管理员
     */
    @PreAuthorize("@ss.hasPermi('adminer:show_adminer:edit')")
    @Log(title = "管理员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody adminer adminer)
    {
        return toAjax(adminerService.updateadminer(adminer));
    }

    /**
     * 删除管理员
     */
    @PreAuthorize("@ss.hasPermi('adminer:show_adminer:remove')")
    @Log(title = "管理员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{adIds}")
    public AjaxResult remove(@PathVariable Long[] adIds)
    {
        return toAjax(adminerService.deleteadminerByIds(adIds));
    }
}
