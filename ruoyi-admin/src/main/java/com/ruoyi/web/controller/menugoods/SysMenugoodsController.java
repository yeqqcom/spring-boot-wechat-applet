package com.ruoyi.web.controller.menugoods;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.menugoods.domain.SysMenugoods;
import com.ruoyi.menugoods.service.ISysMenugoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 菜单中商品Controller
 * 
 * @author yhh
 * @date 2020-12-23
 */
@RestController
@RequestMapping("/menugoods/show_menugoods")
public class SysMenugoodsController extends BaseController
{
    @Autowired
    private ISysMenugoodsService sysMenugoodsService;

    /**
     * 查询菜单中商品列表
     */
    @PreAuthorize("@ss.hasPermi('menugoods:show_menugoods:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysMenugoods sysMenugoods)
    {
        startPage();
        List<SysMenugoods> list = sysMenugoodsService.selectSysMenugoodsList(sysMenugoods);
        return getDataTable(list);
    }

    /**
     * 导出菜单中商品列表
     */
    @PreAuthorize("@ss.hasPermi('menugoods:show_menugoods:export')")
    @Log(title = "菜单中商品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysMenugoods sysMenugoods)
    {
        List<SysMenugoods> list = sysMenugoodsService.selectSysMenugoodsList(sysMenugoods);
        ExcelUtil<SysMenugoods> util = new ExcelUtil<SysMenugoods>(SysMenugoods.class);
        return util.exportExcel(list, "show_menugoods");
    }

    /**
     * 获取菜单中商品详细信息
     */
    @PreAuthorize("@ss.hasPermi('menugoods:show_menugoods:query')")
    @GetMapping(value = "/{menuId}")
    public AjaxResult getInfo(@PathVariable("menuId") Long menuId)
    {
        return AjaxResult.success(sysMenugoodsService.selectSysMenugoodsById(menuId));
    }

    /**
     * 新增菜单中商品
     */
    @PreAuthorize("@ss.hasPermi('menugoods:show_menugoods:add')")
    @Log(title = "菜单中商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysMenugoods sysMenugoods)
    {
        return toAjax(sysMenugoodsService.insertSysMenugoods(sysMenugoods));
    }

    /**
     * 修改菜单中商品
     */
    @PreAuthorize("@ss.hasPermi('menugoods:show_menugoods:edit')")
    @Log(title = "菜单中商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysMenugoods sysMenugoods)
    {
        return toAjax(sysMenugoodsService.updateSysMenugoods(sysMenugoods));
    }

    /**
     * 删除菜单中商品
     */
    @PreAuthorize("@ss.hasPermi('menugoods:show_menugoods:remove')")
    @Log(title = "菜单中商品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{menuIds}")
    public AjaxResult remove(@PathVariable Long[] menuIds)
    {
        return toAjax(sysMenugoodsService.deleteSysMenugoodsByIds(menuIds));
    }
}
