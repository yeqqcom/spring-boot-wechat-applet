package com.ruoyi.address.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.address.mapper.SysAddressMapper;
import com.ruoyi.address.domain.SysAddress;
import com.ruoyi.address.service.ISysAddressService;

/**
 * 用户地址Service业务层处理
 * 
 * @author yhh
 * @date 2020-12-22
 */
@Service
public class SysAddressServiceImpl implements ISysAddressService 
{
    @Autowired
    private SysAddressMapper sysAddressMapper;

    /**
     * 查询用户地址
     * 
     * @param cusId 用户地址ID
     * @return 用户地址
     */
    @Override
    public SysAddress selectSysAddressById(Long cusId)
    {
        return sysAddressMapper.selectSysAddressById(cusId);
    }

    /**
     * 查询用户地址列表
     * 
     * @param sysAddress 用户地址
     * @return 用户地址
     */
    @Override
    public List<SysAddress> selectSysAddressList(SysAddress sysAddress)
    {
        return sysAddressMapper.selectSysAddressList(sysAddress);
    }

    /**
     * 新增用户地址
     * 
     * @param sysAddress 用户地址
     * @return 结果
     */
    @Override
    public int insertSysAddress(SysAddress sysAddress)
    {
        return sysAddressMapper.insertSysAddress(sysAddress);
    }

    /**
     * 修改用户地址
     * 
     * @param sysAddress 用户地址
     * @return 结果
     */
    @Override
    public int updateSysAddress(SysAddress sysAddress)
    {
        return sysAddressMapper.updateSysAddress(sysAddress);
    }

    /**
     * 批量删除用户地址
     * 
     * @param cusIds 需要删除的用户地址ID
     * @return 结果
     */
    @Override
    public int deleteSysAddressByIds(Long[] cusIds)
    {
        return sysAddressMapper.deleteSysAddressByIds(cusIds);
    }

    /**
     * 删除用户地址信息
     * 
     * @param cusId 用户地址ID
     * @return 结果
     */
    @Override
    public int deleteSysAddressById(Long cusId)
    {
        return sysAddressMapper.deleteSysAddressById(cusId);
    }
}
