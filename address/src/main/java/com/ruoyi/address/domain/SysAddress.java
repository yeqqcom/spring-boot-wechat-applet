package com.ruoyi.address.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户地址对象 sys_address
 * 
 * @author yhh
 * @date 2020-12-22
 */
public class SysAddress extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 客户编号 */
    @Excel(name = "客户编号")
    private Long cusId;

    /** 名称 */
    @Excel(name = "名称")
    private String cusName;

    /** 性别 */
    @Excel(name = "性别")
    private String cusSex;

    /** 电话 */
    @Excel(name = "电话")
    private String cusTel;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String detailaddress;

    public void setCusId(Long cusId) 
    {
        this.cusId = cusId;
    }

    public Long getCusId() 
    {
        return cusId;
    }
    public void setCusName(String cusName) 
    {
        this.cusName = cusName;
    }

    public String getCusName() 
    {
        return cusName;
    }
    public void setCusSex(String cusSex) 
    {
        this.cusSex = cusSex;
    }

    public String getCusSex() 
    {
        return cusSex;
    }
    public void setCusTel(String cusTel) 
    {
        this.cusTel = cusTel;
    }

    public String getCusTel() 
    {
        return cusTel;
    }
    public void setDetailaddress(String detailaddress) 
    {
        this.detailaddress = detailaddress;
    }

    public String getDetailaddress() 
    {
        return detailaddress;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cusId", getCusId())
            .append("cusName", getCusName())
            .append("cusSex", getCusSex())
            .append("cusTel", getCusTel())
            .append("detailaddress", getDetailaddress())
            .toString();
    }
}
