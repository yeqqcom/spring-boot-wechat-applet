package com.ruoyi.address.mapper;

import java.util.List;
import com.ruoyi.address.domain.SysAddress;

/**
 * 用户地址Mapper接口
 * 
 * @author yhh
 * @date 2020-12-22
 */
public interface SysAddressMapper 
{
    /**
     * 查询用户地址
     * 
     * @param cusId 用户地址ID
     * @return 用户地址
     */
    public SysAddress selectSysAddressById(Long cusId);

    /**
     * 查询用户地址列表
     * 
     * @param sysAddress 用户地址
     * @return 用户地址集合
     */
    public List<SysAddress> selectSysAddressList(SysAddress sysAddress);

    /**
     * 新增用户地址
     * 
     * @param sysAddress 用户地址
     * @return 结果
     */
    public int insertSysAddress(SysAddress sysAddress);

    /**
     * 修改用户地址
     * 
     * @param sysAddress 用户地址
     * @return 结果
     */
    public int updateSysAddress(SysAddress sysAddress);

    /**
     * 删除用户地址
     * 
     * @param cusId 用户地址ID
     * @return 结果
     */
    public int deleteSysAddressById(Long cusId);

    /**
     * 批量删除用户地址
     * 
     * @param cusIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysAddressByIds(Long[] cusIds);
}
