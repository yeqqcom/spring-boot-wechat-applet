package com.ruoyi.discount.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.discount.mapper.SysDiscountMapper;
import com.ruoyi.discount.domain.SysDiscount;
import com.ruoyi.discount.service.ISysDiscountService;

/**
 * 优惠券Service业务层处理
 * 
 * @author yhh
 * @date 2020-12-22
 */
@Service
public class SysDiscountServiceImpl implements ISysDiscountService 
{
    @Autowired
    private SysDiscountMapper sysDiscountMapper;

    /**
     * 查询优惠券
     * 
     * @param discountId 优惠券ID
     * @return 优惠券
     */
    @Override
    public SysDiscount selectSysDiscountById(Long discountId)
    {
        return sysDiscountMapper.selectSysDiscountById(discountId);
    }

    /**
     * 查询优惠券列表
     * 
     * @param sysDiscount 优惠券
     * @return 优惠券
     */
    @Override
    public List<SysDiscount> selectSysDiscountList(SysDiscount sysDiscount)
    {
        return sysDiscountMapper.selectSysDiscountList(sysDiscount);
    }

    /**
     * 新增优惠券
     * 
     * @param sysDiscount 优惠券
     * @return 结果
     */
    @Override
    public int insertSysDiscount(SysDiscount sysDiscount)
    {
        return sysDiscountMapper.insertSysDiscount(sysDiscount);
    }

    /**
     * 修改优惠券
     * 
     * @param sysDiscount 优惠券
     * @return 结果
     */
    @Override
    public int updateSysDiscount(SysDiscount sysDiscount)
    {
        return sysDiscountMapper.updateSysDiscount(sysDiscount);
    }

    /**
     * 批量删除优惠券
     * 
     * @param discountIds 需要删除的优惠券ID
     * @return 结果
     */
    @Override
    public int deleteSysDiscountByIds(Long[] discountIds)
    {
        return sysDiscountMapper.deleteSysDiscountByIds(discountIds);
    }

    /**
     * 删除优惠券信息
     * 
     * @param discountId 优惠券ID
     * @return 结果
     */
    @Override
    public int deleteSysDiscountById(Long discountId)
    {
        return sysDiscountMapper.deleteSysDiscountById(discountId);
    }
}
