package com.ruoyi.discount.mapper;

import java.util.List;
import com.ruoyi.discount.domain.SysDiscount;

/**
 * 优惠券Mapper接口
 * 
 * @author yhh
 * @date 2020-12-22
 */
public interface SysDiscountMapper 
{
    /**
     * 查询优惠券
     * 
     * @param discountId 优惠券ID
     * @return 优惠券
     */
    public SysDiscount selectSysDiscountById(Long discountId);

    /**
     * 查询优惠券列表
     * 
     * @param sysDiscount 优惠券
     * @return 优惠券集合
     */
    public List<SysDiscount> selectSysDiscountList(SysDiscount sysDiscount);

    /**
     * 新增优惠券
     * 
     * @param sysDiscount 优惠券
     * @return 结果
     */
    public int insertSysDiscount(SysDiscount sysDiscount);

    /**
     * 修改优惠券
     * 
     * @param sysDiscount 优惠券
     * @return 结果
     */
    public int updateSysDiscount(SysDiscount sysDiscount);

    /**
     * 删除优惠券
     * 
     * @param discountId 优惠券ID
     * @return 结果
     */
    public int deleteSysDiscountById(Long discountId);

    /**
     * 批量删除优惠券
     * 
     * @param discountIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysDiscountByIds(Long[] discountIds);
}
