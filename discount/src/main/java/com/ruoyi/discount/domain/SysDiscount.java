package com.ruoyi.discount.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 优惠券对象 sys_discount
 * 
 * @author yhh
 * @date 2020-12-22
 */
public class SysDiscount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 优惠券编号 */
    @Excel(name = "优惠券编号")
    private Long discountId;

    /** 优惠券名称 */
    @Excel(name = "优惠券名称")
    private String discountName;

    /** 优惠介绍 */
    @Excel(name = "优惠介绍")
    private String discountDetail;

    /** 优惠开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "优惠开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date discountStatime;

    /** 优惠结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "优惠结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date discountFintime;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date discountMaketime;

    public void setDiscountId(Long discountId) 
    {
        this.discountId = discountId;
    }

    public Long getDiscountId() 
    {
        return discountId;
    }
    public void setDiscountName(String discountName) 
    {
        this.discountName = discountName;
    }

    public String getDiscountName() 
    {
        return discountName;
    }
    public void setDiscountDetail(String discountDetail) 
    {
        this.discountDetail = discountDetail;
    }

    public String getDiscountDetail() 
    {
        return discountDetail;
    }
    public void setDiscountStatime(Date discountStatime) 
    {
        this.discountStatime = discountStatime;
    }

    public Date getDiscountStatime() 
    {
        return discountStatime;
    }
    public void setDiscountFintime(Date discountFintime) 
    {
        this.discountFintime = discountFintime;
    }

    public Date getDiscountFintime() 
    {
        return discountFintime;
    }
    public void setDiscountMaketime(Date discountMaketime) 
    {
        this.discountMaketime = discountMaketime;
    }

    public Date getDiscountMaketime() 
    {
        return discountMaketime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("discountId", getDiscountId())
            .append("discountName", getDiscountName())
            .append("discountDetail", getDiscountDetail())
            .append("discountStatime", getDiscountStatime())
            .append("discountFintime", getDiscountFintime())
            .append("discountMaketime", getDiscountMaketime())
            .toString();
    }
}
