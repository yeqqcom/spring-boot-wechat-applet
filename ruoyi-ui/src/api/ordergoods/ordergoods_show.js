import request from '@/utils/request'

// 查询客户个人信息列表
export function listOrdergoods_show(query) {
  return request({
    url: '/ordergoods/ordergoods_show/list',
    method: 'get',
    params: query
  })
}

// 查询客户个人信息详细
export function getOrdergoods_show(cusId) {
  return request({
    url: '/ordergoods/ordergoods_show/' + cusId,
    method: 'get'
  })
}

// 新增客户个人信息
export function addOrdergoods_show(data) {
  return request({
    url: '/ordergoods/ordergoods_show',
    method: 'post',
    data: data
  })
}

// 修改客户个人信息
export function updateOrdergoods_show(data) {
  return request({
    url: '/ordergoods/ordergoods_show',
    method: 'put',
    data: data
  })
}

// 删除客户个人信息
export function delOrdergoods_show(cusId) {
  return request({
    url: '/ordergoods/ordergoods_show/' + cusId,
    method: 'delete'
  })
}

// 导出客户个人信息
export function exportOrdergoods_show(query) {
  return request({
    url: '/ordergoods/ordergoods_show/export',
    method: 'get',
    params: query
  })
}