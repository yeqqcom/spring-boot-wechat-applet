import request from '@/utils/request'

// 查询管理员列表
export function listShow_adminer(query) {
  return request({
    url: '/adminer/show_adminer/list',
    method: 'get',
    params: query
  })
}

// 查询管理员详细
export function getShow_adminer(adId) {
  return request({
    url: '/adminer/show_adminer/' + adId,
    method: 'get'
  })
}

// 新增管理员
export function addShow_adminer(data) {
  return request({
    url: '/adminer/show_adminer',
    method: 'post',
    data: data
  })
}

// 修改管理员
export function updateShow_adminer(data) {
  return request({
    url: '/adminer/show_adminer',
    method: 'put',
    data: data
  })
}

// 删除管理员
export function delShow_adminer(adId) {
  return request({
    url: '/adminer/show_adminer/' + adId,
    method: 'delete'
  })
}

// 导出管理员
export function exportShow_adminer(query) {
  return request({
    url: '/adminer/show_adminer/export',
    method: 'get',
    params: query
  })
}