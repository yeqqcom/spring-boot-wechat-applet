import request from '@/utils/request'

// 查询商品列表
export function listShow_goods_new(query) {
  return request({
    url: '/goods_new/show_goods_new/list',
    method: 'get',
    params: query
  })
}

// 查询商品详细
export function getShow_goods_new(goodsId) {
  return request({
    url: '/goods_new/show_goods_new/' + goodsId,
    method: 'get'
  })
}

// 新增商品
export function addShow_goods_new(data) {
  return request({
    url: '/goods_new/show_goods_new',
    method: 'post',
    data: data
  })
}

// 修改商品
export function updateShow_goods_new(data) {
  return request({
    url: '/goods_new/show_goods_new',
    method: 'put',
    data: data
  })
}

// 删除商品
export function delShow_goods_new(goodsId) {
  return request({
    url: '/goods_new/show_goods_new/' + goodsId,
    method: 'delete'
  })
}

// 导出商品
export function exportShow_goods_new(query) {
  return request({
    url: '/goods_new/show_goods_new/export',
    method: 'get',
    params: query
  })
}