import request from '@/utils/request'

// 查询总订单列表
export function listShow_order_new(query) {
  return request({
    url: '/order_new/show_order_new/list',
    method: 'get',
    params: query
  })
}

// 查询总订单详细
export function getShow_order_new(orderid) {
  return request({
    url: '/order_new/show_order_new/' + orderid,
    method: 'get'
  })
}

// 新增总订单
export function addShow_order_new(data) {
  return request({
    url: '/order_new/show_order_new',
    method: 'post',
    data: data
  })
}

// 修改总订单
export function updateShow_order_new(data) {
  return request({
    url: '/order_new/show_order_new',
    method: 'put',
    data: data
  })
}

// 删除总订单
export function delShow_order_new(orderid) {
  return request({
    url: '/order_new/show_order_new/' + orderid,
    method: 'delete'
  })
}

// 导出总订单
export function exportShow_order_new(query) {
  return request({
    url: '/order_new/show_order_new/export',
    method: 'get',
    params: query
  })
}