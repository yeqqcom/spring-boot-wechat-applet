import request from '@/utils/request'

// 查询商品规格列表
export function listShow_goodstant(query) {
  return request({
    url: '/goodstant/show_goodstant/list',
    method: 'get',
    params: query
  })
}

// 查询商品规格详细
export function getShow_goodstant(stanId) {
  return request({
    url: '/goodstant/show_goodstant/' + stanId,
    method: 'get'
  })
}

// 新增商品规格
export function addShow_goodstant(data) {
  return request({
    url: '/goodstant/show_goodstant',
    method: 'post',
    data: data
  })
}

// 修改商品规格
export function updateShow_goodstant(data) {
  return request({
    url: '/goodstant/show_goodstant',
    method: 'put',
    data: data
  })
}

// 删除商品规格
export function delShow_goodstant(stanId) {
  return request({
    url: '/goodstant/show_goodstant/' + stanId,
    method: 'delete'
  })
}

// 导出商品规格
export function exportShow_goodstant(query) {
  return request({
    url: '/goodstant/show_goodstant/export',
    method: 'get',
    params: query
  })
}