import request from '@/utils/request'

// 查询菜单中商品列表
export function listShow_menugoods(query) {
  return request({
    url: '/menugoods/show_menugoods/list',
    method: 'get',
    params: query
  })
}

// 查询菜单中商品详细
export function getShow_menugoods(menuId) {
  return request({
    url: '/menugoods/show_menugoods/' + menuId,
    method: 'get'
  })
}

// 新增菜单中商品
export function addShow_menugoods(data) {
  return request({
    url: '/menugoods/show_menugoods',
    method: 'post',
    data: data
  })
}

// 修改菜单中商品
export function updateShow_menugoods(data) {
  return request({
    url: '/menugoods/show_menugoods',
    method: 'put',
    data: data
  })
}

// 删除菜单中商品
export function delShow_menugoods(menuId) {
  return request({
    url: '/menugoods/show_menugoods/' + menuId,
    method: 'delete'
  })
}

// 导出菜单中商品
export function exportShow_menugoods(query) {
  return request({
    url: '/menugoods/show_menugoods/export',
    method: 'get',
    params: query
  })
}