import request from '@/utils/request'

// 查询画报列表
export function listShow_bigbanner(query) {
  return request({
    url: '/bingbanner/show_bigbanner/list',
    method: 'get',
    params: query
  })
}

// 查询画报详细
export function getShow_bigbanner(bannerId) {
  return request({
    url: '/bingbanner/show_bigbanner/' + bannerId,
    method: 'get'
  })
}

// 新增画报
export function addShow_bigbanner(data) {
  return request({
    url: '/bingbanner/show_bigbanner',
    method: 'post',
    data: data
  })
}

// 修改画报
export function updateShow_bigbanner(data) {
  return request({
    url: '/bingbanner/show_bigbanner',
    method: 'put',
    data: data
  })
}

// 删除画报
export function delShow_bigbanner(bannerId) {
  return request({
    url: '/bingbanner/show_bigbanner/' + bannerId,
    method: 'delete'
  })
}

// 导出画报
export function exportShow_bigbanner(query) {
  return request({
    url: '/bingbanner/show_bigbanner/export',
    method: 'get',
    params: query
  })
}