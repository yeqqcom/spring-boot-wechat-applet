import request from '@/utils/request'

// 查询分级菜单列表
export function listShow_catemenu(query) {
  return request({
    url: '/catemenu/show_catemenu/list',
    method: 'get',
    params: query
  })
}

// 查询分级菜单详细
export function getShow_catemenu(menuId) {
  return request({
    url: '/catemenu/show_catemenu/' + menuId,
    method: 'get'
  })
}

// 新增分级菜单
export function addShow_catemenu(data) {
  return request({
    url: '/catemenu/show_catemenu',
    method: 'post',
    data: data
  })
}

// 修改分级菜单
export function updateShow_catemenu(data) {
  return request({
    url: '/catemenu/show_catemenu',
    method: 'put',
    data: data
  })
}

// 删除分级菜单
export function delShow_catemenu(menuId) {
  return request({
    url: '/catemenu/show_catemenu/' + menuId,
    method: 'delete'
  })
}

// 导出分级菜单
export function exportShow_catemenu(query) {
  return request({
    url: '/catemenu/show_catemenu/export',
    method: 'get',
    params: query
  })
}