import request from '@/utils/request'

// 查询商品列表
export function listShow_goods(query) {
  return request({
    url: '/goods/show_goods/list',
    method: 'get',
    params: query
  })
}

// 查询商品详细
export function getShow_goods(goodsId) {
  return request({
    url: '/goods/show_goods/' + goodsId,
    method: 'get'
  })
}

// 新增商品
export function addShow_goods(data) {
  return request({
    url: '/goods/show_goods',
    method: 'post',
    data: data
  })
}

// 修改商品
export function updateShow_goods(data) {
  return request({
    url: '/goods/show_goods',
    method: 'put',
    data: data
  })
}

// 删除商品
export function delShow_goods(goodsId) {
  return request({
    url: '/goods/show_goods/' + goodsId,
    method: 'delete'
  })
}

// 导出商品
export function exportShow_goods(query) {
  return request({
    url: '/goods/show_goods/export',
    method: 'get',
    params: query
  })
}