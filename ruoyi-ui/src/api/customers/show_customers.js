import request from '@/utils/request'

// 查询客户个人信息列表
export function listShow_customers(query) {
  return request({
    url: '/customers/show_customers/list',
    method: 'get',
    params: query
  })
}

// 查询客户个人信息详细
export function getShow_customers(cusId) {
  return request({
    url: '/customers/show_customers/' + cusId,
    method: 'get'
  })
}

// 新增客户个人信息
export function addShow_customers(data) {
  return request({
    url: '/customers/show_customers',
    method: 'post',
    data: data
  })
}

// 修改客户个人信息
export function updateShow_customers(data) {
  return request({
    url: '/customers/show_customers',
    method: 'put',
    data: data
  })
}

// 删除客户个人信息
export function delShow_customers(cusId) {
  return request({
    url: '/customers/show_customers/' + cusId,
    method: 'delete'
  })
}

// 导出客户个人信息
export function exportShow_customers(query) {
  return request({
    url: '/customers/show_customers/export',
    method: 'get',
    params: query
  })
}