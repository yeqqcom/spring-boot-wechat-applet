import request from '@/utils/request'

// 查询订单列表
export function listShow_order(query) {
  return request({
    url: '/order/show_order/list',
    method: 'get',
    params: query
  })
}

// 查询订单详细
export function getShow_order(orderId) {
  return request({
    url: '/order/show_order/' + orderId,
    method: 'get'
  })
}

// 新增订单
export function addShow_order(data) {
  return request({
    url: '/order/show_order',
    method: 'post',
    data: data
  })
}

// 修改订单
export function updateShow_order(data) {
  return request({
    url: '/order/show_order',
    method: 'put',
    data: data
  })
}

// 删除订单
export function delShow_order(orderId) {
  return request({
    url: '/order/show_order/' + orderId,
    method: 'delete'
  })
}

// 导出订单
export function exportShow_order(query) {
  return request({
    url: '/order/show_order/export',
    method: 'get',
    params: query
  })
}