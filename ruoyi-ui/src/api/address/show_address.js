import request from '@/utils/request'

// 查询用户地址列表
export function listShow_address(query) {
  return request({
    url: '/address/show_address/list',
    method: 'get',
    params: query
  })
}

// 查询用户地址详细
export function getShow_address(cusId) {
  return request({
    url: '/address/show_address/' + cusId,
    method: 'get'
  })
}

// 新增用户地址
export function addShow_address(data) {
  return request({
    url: '/address/show_address',
    method: 'post',
    data: data
  })
}

// 修改用户地址
export function updateShow_address(data) {
  return request({
    url: '/address/show_address',
    method: 'put',
    data: data
  })
}

// 删除用户地址
export function delShow_address(cusId) {
  return request({
    url: '/address/show_address/' + cusId,
    method: 'delete'
  })
}

// 导出用户地址
export function exportShow_address(query) {
  return request({
    url: '/address/show_address/export',
    method: 'get',
    params: query
  })
}