import request from '@/utils/request'

// 查询优惠券列表
export function listShow_discount(query) {
  return request({
    url: '/discount/show_discount/list',
    method: 'get',
    params: query
  })
}

// 查询优惠券详细
export function getShow_discount(discountId) {
  return request({
    url: '/discount/show_discount/' + discountId,
    method: 'get'
  })
}

// 新增优惠券
export function addShow_discount(data) {
  return request({
    url: '/discount/show_discount',
    method: 'post',
    data: data
  })
}

// 修改优惠券
export function updateShow_discount(data) {
  return request({
    url: '/discount/show_discount',
    method: 'put',
    data: data
  })
}

// 删除优惠券
export function delShow_discount(discountId) {
  return request({
    url: '/discount/show_discount/' + discountId,
    method: 'delete'
  })
}

// 导出优惠券
export function exportShow_discount(query) {
  return request({
    url: '/discount/show_discount/export',
    method: 'get',
    params: query
  })
}