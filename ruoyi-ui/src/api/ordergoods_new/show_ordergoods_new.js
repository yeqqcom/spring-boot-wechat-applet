import request from '@/utils/request'

// 查询订单详细商品列表
export function listShow_ordergoods_new(query) {
  return request({
    url: '/ordergoods_new/show_ordergoods_new/list',
    method: 'get',
    params: query
  })
}

// 查询订单详细商品详细
export function getShow_ordergoods_new(username) {
  return request({
    url: '/ordergoods_new/show_ordergoods_new/' + username,
    method: 'get'
  })
}

// 新增订单详细商品
export function addShow_ordergoods_new(data) {
  return request({
    url: '/ordergoods_new/show_ordergoods_new',
    method: 'post',
    data: data
  })
}

// 修改订单详细商品
export function updateShow_ordergoods_new(data) {
  return request({
    url: '/ordergoods_new/show_ordergoods_new',
    method: 'put',
    data: data
  })
}

// 删除订单详细商品
export function delShow_ordergoods_new(ids) {
  return request({
    url: '/ordergoods_new/show_ordergoods_new/' + ids,
    method: 'delete'
  })
}

// 导出订单详细商品
export function exportShow_ordergoods_new(query) {
  return request({
    url: '/ordergoods_new/show_ordergoods_new/export',
    method: 'get',
    params: query
  })
}
