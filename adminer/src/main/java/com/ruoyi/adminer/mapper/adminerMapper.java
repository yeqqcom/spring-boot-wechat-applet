package com.ruoyi.adminer.mapper;

import java.util.List;
import com.ruoyi.adminer.domain.adminer;

/**
 * 管理员Mapper接口
 * 
 * @author yhh
 * @date 2020-12-22
 */
public interface adminerMapper 
{
    /**
     * 查询管理员
     * 
     * @param adId 管理员ID
     * @return 管理员
     */
    public adminer selectadminerById(Long adId);

    /**
     * 查询管理员列表
     * 
     * @param adminer 管理员
     * @return 管理员集合
     */
    public List<adminer> selectadminerList(adminer adminer);

    /**
     * 新增管理员
     * 
     * @param adminer 管理员
     * @return 结果
     */
    public int insertadminer(adminer adminer);

    /**
     * 修改管理员
     * 
     * @param adminer 管理员
     * @return 结果
     */
    public int updateadminer(adminer adminer);

    /**
     * 删除管理员
     * 
     * @param adId 管理员ID
     * @return 结果
     */
    public int deleteadminerById(Long adId);

    /**
     * 批量删除管理员
     * 
     * @param adIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteadminerByIds(Long[] adIds);
}
