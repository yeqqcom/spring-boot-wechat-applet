package com.ruoyi.adminer.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 管理员对象 sys_adminer
 * 
 * @author yhh
 * @date 2020-12-22
 */
public class adminer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 管理员编号 */
    @Excel(name = "管理员编号")
    private Long adId;

    /** 管理员头像 */
    @Excel(name = "管理员头像")
    private String adImg;

    /** 管理员昵称 */
    @Excel(name = "管理员昵称")
    private String adName;

    /** 管理员职位 */
    @Excel(name = "管理员职位")
    private String adRole;

    public void setAdId(Long adId) 
    {
        this.adId = adId;
    }

    public Long getAdId() 
    {
        return adId;
    }
    public void setAdImg(String adImg) 
    {
        this.adImg = adImg;
    }

    public String getAdImg() 
    {
        return adImg;
    }
    public void setAdName(String adName) 
    {
        this.adName = adName;
    }

    public String getAdName() 
    {
        return adName;
    }
    public void setAdRole(String adRole) 
    {
        this.adRole = adRole;
    }

    public String getAdRole() 
    {
        return adRole;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("adId", getAdId())
            .append("adImg", getAdImg())
            .append("adName", getAdName())
            .append("adRole", getAdRole())
            .toString();
    }
}
