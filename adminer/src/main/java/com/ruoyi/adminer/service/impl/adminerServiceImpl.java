package com.ruoyi.adminer.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.adminer.mapper.adminerMapper;
import com.ruoyi.adminer.domain.adminer;
import com.ruoyi.adminer.service.IadminerService;

/**
 * 管理员Service业务层处理
 * 
 * @author yhh
 * @date 2020-12-22
 */
@Service
public class adminerServiceImpl implements IadminerService 
{
    @Autowired
    private adminerMapper adminerMapper;

    /**
     * 查询管理员
     * 
     * @param adId 管理员ID
     * @return 管理员
     */
    @Override
    public adminer selectadminerById(Long adId)
    {
        return adminerMapper.selectadminerById(adId);
    }

    /**
     * 查询管理员列表
     * 
     * @param adminer 管理员
     * @return 管理员
     */
    @Override
    public List<adminer> selectadminerList(adminer adminer)
    {
        return adminerMapper.selectadminerList(adminer);
    }

    /**
     * 新增管理员
     * 
     * @param adminer 管理员
     * @return 结果
     */
    @Override
    public int insertadminer(adminer adminer)
    {
        return adminerMapper.insertadminer(adminer);
    }

    /**
     * 修改管理员
     * 
     * @param adminer 管理员
     * @return 结果
     */
    @Override
    public int updateadminer(adminer adminer)
    {
        return adminerMapper.updateadminer(adminer);
    }

    /**
     * 批量删除管理员
     * 
     * @param adIds 需要删除的管理员ID
     * @return 结果
     */
    @Override
    public int deleteadminerByIds(Long[] adIds)
    {
        return adminerMapper.deleteadminerByIds(adIds);
    }

    /**
     * 删除管理员信息
     * 
     * @param adId 管理员ID
     * @return 结果
     */
    @Override
    public int deleteadminerById(Long adId)
    {
        return adminerMapper.deleteadminerById(adId);
    }
}
