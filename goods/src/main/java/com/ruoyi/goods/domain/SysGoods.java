package com.ruoyi.goods.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品对象 sys_goods
 * 
 * @author yhh
 * @date 2020-12-23
 */
public class SysGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private Long goodsId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodsName;

    /** 商品图片 */
    @Excel(name = "商品图片")
    private String goodsImg;

    /** 商品状态 */
    @Excel(name = "商品状态")
    private String goodsState;

    /** 商品库存 */
    @Excel(name = "商品库存")
    private Long goodsStock;

    /** 商品定价 */
    @Excel(name = "商品定价")
    private BigDecimal goodsPrice;

    /** 计量单位 */
    @Excel(name = "计量单位")
    private String goodsMeasurement;

    /** 商品简介 */
    @Excel(name = "商品简介")
    private String goodsDetail;

    /** 最多下单量 */
    @Excel(name = "最多下单量")
    private Long goodsMaxorder;

    /** 最少下单量 */
    @Excel(name = "最少下单量")
    private Long goodsMinorder;

    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setGoodsImg(String goodsImg) 
    {
        this.goodsImg = goodsImg;
    }

    public String getGoodsImg() 
    {
        return goodsImg;
    }
    public void setGoodsState(String goodsState) 
    {
        this.goodsState = goodsState;
    }

    public String getGoodsState() 
    {
        return goodsState;
    }
    public void setGoodsStock(Long goodsStock) 
    {
        this.goodsStock = goodsStock;
    }

    public Long getGoodsStock() 
    {
        return goodsStock;
    }
    public void setGoodsPrice(BigDecimal goodsPrice) 
    {
        this.goodsPrice = goodsPrice;
    }

    public BigDecimal getGoodsPrice() 
    {
        return goodsPrice;
    }
    public void setGoodsMeasurement(String goodsMeasurement) 
    {
        this.goodsMeasurement = goodsMeasurement;
    }

    public String getGoodsMeasurement() 
    {
        return goodsMeasurement;
    }
    public void setGoodsDetail(String goodsDetail) 
    {
        this.goodsDetail = goodsDetail;
    }

    public String getGoodsDetail() 
    {
        return goodsDetail;
    }
    public void setGoodsMaxorder(Long goodsMaxorder) 
    {
        this.goodsMaxorder = goodsMaxorder;
    }

    public Long getGoodsMaxorder() 
    {
        return goodsMaxorder;
    }
    public void setGoodsMinorder(Long goodsMinorder) 
    {
        this.goodsMinorder = goodsMinorder;
    }

    public Long getGoodsMinorder() 
    {
        return goodsMinorder;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("goodsId", getGoodsId())
            .append("goodsName", getGoodsName())
            .append("goodsImg", getGoodsImg())
            .append("goodsState", getGoodsState())
            .append("goodsStock", getGoodsStock())
            .append("goodsPrice", getGoodsPrice())
            .append("goodsMeasurement", getGoodsMeasurement())
            .append("goodsDetail", getGoodsDetail())
            .append("goodsMaxorder", getGoodsMaxorder())
            .append("goodsMinorder", getGoodsMinorder())
            .toString();
    }
}
