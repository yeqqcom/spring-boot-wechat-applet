package com.ruoyi.ordergoods.mapper;

import java.util.List;
import com.ruoyi.ordergoods.domain.SysCustomerCopy1;

/**
 * 客户个人信息Mapper接口
 * 
 * @author yhh
 * @date 2020-12-25
 */
public interface SysCustomerCopy1Mapper 
{
    /**
     * 查询客户个人信息
     * 
     * @param cusId 客户个人信息ID
     * @return 客户个人信息
     */
    public SysCustomerCopy1 selectSysCustomerCopy1ById(Long cusId);

    /**
     * 查询客户个人信息列表
     * 
     * @param sysCustomerCopy1 客户个人信息
     * @return 客户个人信息集合
     */
    public List<SysCustomerCopy1> selectSysCustomerCopy1List(SysCustomerCopy1 sysCustomerCopy1);

    /**
     * 新增客户个人信息
     * 
     * @param sysCustomerCopy1 客户个人信息
     * @return 结果
     */
    public int insertSysCustomerCopy1(SysCustomerCopy1 sysCustomerCopy1);

    /**
     * 修改客户个人信息
     * 
     * @param sysCustomerCopy1 客户个人信息
     * @return 结果
     */
    public int updateSysCustomerCopy1(SysCustomerCopy1 sysCustomerCopy1);

    /**
     * 删除客户个人信息
     * 
     * @param cusId 客户个人信息ID
     * @return 结果
     */
    public int deleteSysCustomerCopy1ById(Long cusId);

    /**
     * 批量删除客户个人信息
     * 
     * @param cusIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysCustomerCopy1ByIds(Long[] cusIds);
}
