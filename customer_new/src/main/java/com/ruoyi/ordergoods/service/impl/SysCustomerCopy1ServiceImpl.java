package com.ruoyi.ordergoods.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ordergoods.mapper.SysCustomerCopy1Mapper;
import com.ruoyi.ordergoods.domain.SysCustomerCopy1;
import com.ruoyi.ordergoods.service.ISysCustomerCopy1Service;

/**
 * 客户个人信息Service业务层处理
 * 
 * @author yhh
 * @date 2020-12-25
 */
@Service
public class SysCustomerCopy1ServiceImpl implements ISysCustomerCopy1Service 
{
    @Autowired
    private SysCustomerCopy1Mapper sysCustomerCopy1Mapper;

    /**
     * 查询客户个人信息
     * 
     * @param cusId 客户个人信息ID
     * @return 客户个人信息
     */
    @Override
    public SysCustomerCopy1 selectSysCustomerCopy1ById(Long cusId)
    {
        return sysCustomerCopy1Mapper.selectSysCustomerCopy1ById(cusId);
    }

    /**
     * 查询客户个人信息列表
     * 
     * @param sysCustomerCopy1 客户个人信息
     * @return 客户个人信息
     */
    @Override
    public List<SysCustomerCopy1> selectSysCustomerCopy1List(SysCustomerCopy1 sysCustomerCopy1)
    {
        return sysCustomerCopy1Mapper.selectSysCustomerCopy1List(sysCustomerCopy1);
    }


    /**
     * 新增客户个人信息
     * 
     * @param sysCustomerCopy1 客户个人信息
     * @return 结果
     */
    @Override
    public int insertSysCustomerCopy1(SysCustomerCopy1 sysCustomerCopy1)
    {
        return sysCustomerCopy1Mapper.insertSysCustomerCopy1(sysCustomerCopy1);
    }

    /**
     * 修改客户个人信息
     * 
     * @param sysCustomerCopy1 客户个人信息
     * @return 结果
     */
    @Override
    public int updateSysCustomerCopy1(SysCustomerCopy1 sysCustomerCopy1)
    {
        return sysCustomerCopy1Mapper.updateSysCustomerCopy1(sysCustomerCopy1);
    }

    /**
     * 批量删除客户个人信息
     * 
     * @param cusIds 需要删除的客户个人信息ID
     * @return 结果
     */
    @Override
    public int deleteSysCustomerCopy1ByIds(Long[] cusIds)
    {
        return sysCustomerCopy1Mapper.deleteSysCustomerCopy1ByIds(cusIds);
    }

    /**
     * 删除客户个人信息信息
     * 
     * @param cusId 客户个人信息ID
     * @return 结果
     */
    @Override
    public int deleteSysCustomerCopy1ById(Long cusId)
    {
        return sysCustomerCopy1Mapper.deleteSysCustomerCopy1ById(cusId);
    }

}
