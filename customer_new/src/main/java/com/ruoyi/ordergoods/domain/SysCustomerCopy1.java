package com.ruoyi.ordergoods.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 客户个人信息对象 sys_customer_copy1
 * 
 * @author yhh
 * @date 2020-12-25
 */
public class SysCustomerCopy1 extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 客户id */
    private Long cusId;

    /** 客户openid */
    @Excel(name = "客户openid")
    private String openid;

    /** 客户头像 */
    @Excel(name = "客户头像")
    private String cusImg;

    /** 客户微信昵称 */
    @Excel(name = "客户微信昵称")
    private String cusName;

    /** 客户电话 */
    @Excel(name = "客户电话")
    private String cusTel;

    /** 客户性别 */
    @Excel(name = "客户性别")
    private String cusSex;

    /** 客户状态 */
    @Excel(name = "客户状态")
    private String cusState;

    public void setCusId(Long cusId) 
    {
        this.cusId = cusId;
    }

    public Long getCusId() 
    {
        return cusId;
    }
    public void setOpenid(String openid) 
    {
        this.openid = openid;
    }

    public String getOpenid() 
    {
        return openid;
    }
    public void setCusImg(String cusImg) 
    {
        this.cusImg = cusImg;
    }

    public String getCusImg() 
    {
        return cusImg;
    }
    public void setCusName(String cusName) 
    {
        this.cusName = cusName;
    }

    public String getCusName() 
    {
        return cusName;
    }
    public void setCusTel(String cusTel) 
    {
        this.cusTel = cusTel;
    }

    public String getCusTel() 
    {
        return cusTel;
    }
    public void setCusSex(String cusSex) 
    {
        this.cusSex = cusSex;
    }

    public String getCusSex() 
    {
        return cusSex;
    }
    public void setCusState(String cusState) 
    {
        this.cusState = cusState;
    }

    public String getCusState() 
    {
        return cusState;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cusId", getCusId())
            .append("openid", getOpenid())
            .append("cusImg", getCusImg())
            .append("cusName", getCusName())
            .append("cusTel", getCusTel())
            .append("cusSex", getCusSex())
            .append("cusState", getCusState())
            .toString();
    }
}
