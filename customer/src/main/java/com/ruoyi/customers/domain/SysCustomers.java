package com.ruoyi.customers.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 客户个人信息对象 sys_customer
 * 
 * @author yhh
 * @date 2020-12-23
 */
public class SysCustomers extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 客户编号 */
    @Excel(name = "客户编号")
    private Long cusId;

    /** 微信id */
    @Excel(name = "微信id")
    private String openid;

    /** 头像 */
    @Excel(name = "头像")
    private String cusImg;

    /** 名称 */
    @Excel(name = "名称")
    private String cusName;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String cusTel;

    /** 性别 */
    @Excel(name = "性别")
    private String cusSex;

    /** 等级 */
    @Excel(name = "等级")
    private String cusLevel;

    /** 积分 */
    @Excel(name = "积分")
    private Long cusPoint;

    /** 状态 */
    @Excel(name = "状态")
    private String cusState;

    public void setCusId(Long cusId) 
    {
        this.cusId = cusId;
    }

    public Long getCusId() 
    {
        return cusId;
    }
    public void setOpenid(String openid) 
    {
        this.openid = openid;
    }

    public String getOpenid() 
    {
        return openid;
    }
    public void setCusImg(String cusImg) 
    {
        this.cusImg = cusImg;
    }

    public String getCusImg() 
    {
        return cusImg;
    }
    public void setCusName(String cusName) 
    {
        this.cusName = cusName;
    }

    public String getCusName() 
    {
        return cusName;
    }
    public void setCusTel(String cusTel) 
    {
        this.cusTel = cusTel;
    }

    public String getCusTel() 
    {
        return cusTel;
    }
    public void setCusSex(String cusSex) 
    {
        this.cusSex = cusSex;
    }

    public String getCusSex() 
    {
        return cusSex;
    }
    public void setCusLevel(String cusLevel) 
    {
        this.cusLevel = cusLevel;
    }

    public String getCusLevel() 
    {
        return cusLevel;
    }
    public void setCusPoint(Long cusPoint) 
    {
        this.cusPoint = cusPoint;
    }

    public Long getCusPoint() 
    {
        return cusPoint;
    }
    public void setCusState(String cusState) 
    {
        this.cusState = cusState;
    }

    public String getCusState() 
    {
        return cusState;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cusId", getCusId())
            .append("openid", getOpenid())
            .append("cusImg", getCusImg())
            .append("cusName", getCusName())
            .append("cusTel", getCusTel())
            .append("cusSex", getCusSex())
            .append("cusLevel", getCusLevel())
            .append("cusPoint", getCusPoint())
            .append("cusState", getCusState())
            .toString();
    }
}
