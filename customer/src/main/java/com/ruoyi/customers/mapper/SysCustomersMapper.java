package com.ruoyi.customers.mapper;

import java.util.List;
import com.ruoyi.customers.domain.SysCustomers;

/**
 * 客户个人信息Mapper接口
 * 
 * @author yhh
 * @date 2020-12-23
 */
public interface SysCustomersMapper 
{
    /**
     * 查询客户个人信息
     * 
     * @param cusId 客户个人信息ID
     * @return 客户个人信息
     */
    public SysCustomers selectSysCustomersById(Long cusId);
    public SysCustomers selectSysCustomersByOpenid(String openid);

    /**
     * 查询客户个人信息列表
     * 
     * @param sysCustomers 客户个人信息
     * @return 客户个人信息集合
     */
    public List<SysCustomers> selectSysCustomersList(SysCustomers sysCustomers);

    /**
     * 新增客户个人信息
     * 
     * @param sysCustomers 客户个人信息
     * @return 结果
     */
    public int insertSysCustomers(SysCustomers sysCustomers);

    /**
     * 修改客户个人信息
     * 
     * @param sysCustomers 客户个人信息
     * @return 结果
     */
    public int updateSysCustomers(SysCustomers sysCustomers);

    /**
     * 删除客户个人信息
     * 
     * @param cusId 客户个人信息ID
     * @return 结果
     */
    public int deleteSysCustomersById(Long cusId);

    /**
     * 批量删除客户个人信息
     * 
     * @param cusIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysCustomersByIds(Long[] cusIds);
}
