package com.ruoyi.customers.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.customers.domain.SysCustomers;

/**
 * 客户个人信息Service接口
 * 
 * @author yhh
 * @date 2020-12-23
 */
public interface ISysCustomersService 
{
    /**
     * 查询客户个人信息
     * 
     * @param cusId 客户个人信息ID
     * @return 客户个人信息
     */
    public SysCustomers selectSysCustomersById(Long cusId);

    /**
     * 查询客户个人信息列表
     * 
     * @param sysCustomers 客户个人信息
     * @return 客户个人信息集合
     */
    public List<SysCustomers> selectSysCustomersList(SysCustomers sysCustomers);

    /**
     * 新增客户个人信息
     * 
     * @param sysCustomers 客户个人信息
     * @return 结果
     */
    public int insertSysCustomers(SysCustomers sysCustomers);

    /**
     * 修改客户个人信息
     * 
     * @param sysCustomers 客户个人信息
     * @return 结果
     */
    public int updateSysCustomers(SysCustomers sysCustomers);

    /**
     * 批量删除客户个人信息
     * 
     * @param cusIds 需要删除的客户个人信息ID
     * @return 结果
     */
    public int deleteSysCustomersByIds(Long[] cusIds);

    /**
     * 删除客户个人信息信息
     * 
     * @param cusId 客户个人信息ID
     * @return 结果
     */
    public int deleteSysCustomersById(Long cusId);

    public JSONObject getSessionKeyOropenid(String code);

    public String sendPost(String url, Map<String, ?> paramMap);

    /**
     * 查询客户个人信息，如果是新用户则同时存入数据库里
     *
     * @param openid 客户个人openid
     * @return 结果
     */
    public SysCustomers getcustomerInfo(String openid,String cusName,String cusImg,String cusSex);

    /**
     * 根据openid获取客户个人信息
     *
     * @param openid 客户个人openid
     * @return 结果
     */

    public SysCustomers getcustomerInfobyopenid(String openid);

    /**
     * 根据openid修改客户个人信息
     *
     * @param openid 客户个人openid
     * @return 结果
     */
    public Boolean changeCustomerInfo(String openid,String phone);

    /**
     * 根据openid修改客户积分和会员等级
     *
     * @param openid 客户个人openid
     * @return 结果
     */
    public Boolean updateCustomerLever(String openid,long point);

}
