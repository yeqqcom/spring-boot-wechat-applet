package com.ruoyi.order_new.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 总订单对象 sys_order_new
 * 
 * @author yeziyin
 * @date 2021-01-02
 */
public class SysOrderNew extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单号 */
    @Excel(name = "订单号")
    private Long orderid;

    /** 下单客户昵称 */
    @Excel(name = "下单客户昵称")
    private String username;

    /** 下单客户openid */
    @Excel(name = "下单客户openid")
    private String useropenid;

    /** 下单客户手机 */
    @Excel(name = "下单客户手机")
    private String userphone;

    /** 所有下单商品数量 */
    @Excel(name = "所有下单商品数量")
    private Long goodsAllnum;

    /** 订单总额 */
    @Excel(name = "订单总额")
    private BigDecimal orderallprice;

    /** 下单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "下单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date ordertime;

    /** 取餐码 */
    @Excel(name = "取餐码")
    private Long pickid;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private String orderfinishtype;

    public void setOrderid(Long orderid) 
    {
        this.orderid = orderid;
    }

    public Long getOrderid() 
    {
        return orderid;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setUseropenid(String useropenid) 
    {
        this.useropenid = useropenid;
    }

    public String getUseropenid() 
    {
        return useropenid;
    }
    public void setUserphone(String userphone) 
    {
        this.userphone = userphone;
    }

    public String getUserphone() 
    {
        return userphone;
    }
    public void setGoodsAllnum(Long goodsAllnum) 
    {
        this.goodsAllnum = goodsAllnum;
    }

    public Long getGoodsAllnum() 
    {
        return goodsAllnum;
    }
    public void setOrderallprice(BigDecimal orderallprice) 
    {
        this.orderallprice = orderallprice;
    }

    public BigDecimal getOrderallprice() 
    {
        return orderallprice;
    }
    public void setOrdertime(Date ordertime) 
    {
        this.ordertime = ordertime;
    }

    public Date getOrdertime() 
    {
        return ordertime;
    }
    public void setPickid(Long pickid) 
    {
        this.pickid = pickid;
    }

    public Long getPickid() 
    {
        return pickid;
    }
    public void setOrderfinishtype(String orderfinishtype) 
    {
        this.orderfinishtype = orderfinishtype;
    }

    public String getOrderfinishtype() 
    {
        return orderfinishtype;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("orderid", getOrderid())
            .append("username", getUsername())
            .append("useropenid", getUseropenid())
            .append("userphone", getUserphone())
            .append("goodsAllnum", getGoodsAllnum())
            .append("orderallprice", getOrderallprice())
            .append("ordertime", getOrdertime())
            .append("pickid", getPickid())
            .append("orderfinishtype", getOrderfinishtype())
            .toString();
    }
}
