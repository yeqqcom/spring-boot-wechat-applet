package com.ruoyi.order_new.domain;

import java.math.BigDecimal;
import java.util.Date;

public class dayStatistical {
    /**今天总销售额* */
    BigDecimal daysales;
    /***今天订单数* */
    int daliyordernum;
    /***日期* */
    Date date;
    /***较前一天销售额变化*/
    BigDecimal comsales;
    /**较前一天订单数变化*/
    int comdaliyordernum;



    String newdate;
    public String getNewdate() {
        return newdate;
    }

    public void setNewdate(String newdate) {
        this.newdate = newdate;
    }

    public BigDecimal getDaysales() {
        return daysales;
    }

    public void setDaysales(BigDecimal daysales) {
        this.daysales = daysales;
    }

    public int getDaliyordernum() {
        return daliyordernum;
    }

    public void setDaliyordernum(int daliyordernum) {
        this.daliyordernum = daliyordernum;
    }

    public int getComdaliyordernum() {
        return comdaliyordernum;
    }

    public void setComdaliyordernum(int comdaliyordernum) {
        this.comdaliyordernum = comdaliyordernum;
    }

    public BigDecimal getComsales() {
        return comsales;
    }

    public void setComsales(BigDecimal comsales) {
        this.comsales = comsales;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
