package com.ruoyi.order_new.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class
OrderStatistical {

    /**总销售额* */
    BigDecimal sales;
    /**总订单数* */
    int totalOrderNum;
    /**总使用下单过用户* */
    int customerNum;
    /**较前一日销售额变化* */
    BigDecimal comsales;
//    /**最近一周的营业情况* */
//    List<dayStatistical> weekStatistical;

    public BigDecimal getComsales() {
        return comsales;
    }

    public void setComsales(BigDecimal comsales) {
        this.comsales = comsales;
    }

    public int getTotalOrderNum() {
        return totalOrderNum;
    }

    public void setTotalOrderNum(int totalOrderNum) {
        this.totalOrderNum = totalOrderNum;
    }

//    public List<dayStatistical> getWeekStatistical() {
//        return weekStatistical;
//    }
//
//    public void setWeekStatistical(List<dayStatistical> weekStatistical) {
//        this.weekStatistical = weekStatistical;
//    }

    public int getCustomerNum() {
        return customerNum;
    }

    public void setCustomerNum(int customerNum) {
        this.customerNum = customerNum;
    }

    public BigDecimal getSales() {
        return sales;
    }

    public void setSales(BigDecimal sales) {
        this.sales = sales;
    }
}
