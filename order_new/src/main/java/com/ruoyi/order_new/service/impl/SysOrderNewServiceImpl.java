package com.ruoyi.order_new.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.ruoyi.order_new.domain.OrderStatistical;
import com.ruoyi.order_new.domain.dayStatistical;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.order_new.mapper.SysOrderNewMapper;
import com.ruoyi.order_new.domain.SysOrderNew;
import com.ruoyi.order_new.service.ISysOrderNewService;
import com.ruoyi.customers.mapper.SysCustomersMapper;
import com.ruoyi.customers.domain.SysCustomers;
import com.ruoyi.customers.service.ISysCustomersService;

/**
 * 总订单Service业务层处理
 * 
 * @author yeziyin
 * @date 2021-01-02
 */
@Service
public class SysOrderNewServiceImpl implements ISysOrderNewService 
{
    @Autowired
    private SysOrderNewMapper sysOrderNewMapper;

//    @Autowired
//    private SysCustomersMapper sysCustomersMapper;



    /**
     * 查询总订单
     * 
     * @param orderid 总订单ID
     * @return 总订单
     */
    @Override
    public SysOrderNew selectSysOrderNewById(Long orderid)
    {
        return sysOrderNewMapper.selectSysOrderNewById(orderid);
    }

    /**
     * 查询总订单列表
     * 
     * @param sysOrderNew 总订单
     * @return 总订单
     */
    @Override
    public List<SysOrderNew> selectSysOrderNewList(SysOrderNew sysOrderNew)
    {
        return sysOrderNewMapper.selectSysOrderNewList(sysOrderNew);
    }


    /**
     * 按openid查询历史所有订单
     *
     * @return 总订单集合
     */
    @Override
    public List<SysOrderNew> selectAllSysOrderNewListByOpenid(String useropenid){
        return sysOrderNewMapper.selectAllSysOrderNewListByOpenid(useropenid);
    }
    /**
     * 按openid查询今天所有订单
     *
     * @return 总订单集合
     */
    @Override
    public List<SysOrderNew> selectTodaySysOrderNewListByOpenid(String useropenid){
        return sysOrderNewMapper.selectTodaySysOrderNewListByOpenid(useropenid);
    }

    /**
     * 新增总订单
     * 
     * @param sysOrderNew 总订单
     * @return 结果
     */
    @Override
    public int insertSysOrderNew(SysOrderNew sysOrderNew)
    {


        return sysOrderNewMapper.insertSysOrderNew(sysOrderNew);
    }

    /**
     * 修改总订单
     * 
     * @param sysOrderNew 总订单
     * @return 结果
     */
    @Override
    public int updateSysOrderNew(SysOrderNew sysOrderNew)
    {
        return sysOrderNewMapper.updateSysOrderNew(sysOrderNew);
    }

    /**
     * 批量删除总订单
     * 
     * @param orderids 需要删除的总订单ID
     * @return 结果
     */
    @Override
    public int deleteSysOrderNewByIds(Long[] orderids)
    {
        return sysOrderNewMapper.deleteSysOrderNewByIds(orderids);
    }

    /**
     * 删除总订单信息
     * 
     * @param orderid 总订单ID
     * @return 结果
     */
    @Override
    public int deleteSysOrderNewById(Long orderid)
    {
        return sysOrderNewMapper.deleteSysOrderNewById(orderid);
    }

    @Override
    public BigDecimal getTotalSales(){
        return sysOrderNewMapper.selectTotalsales();
    }

    @Override
    public List<dayStatistical> getWeeklyDayStatistical(){
        List<dayStatistical> weeklystatical = new ArrayList<dayStatistical>();
        Date date = new Date();
        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        System.out.println(dateFormat.format(date));
        int a[]={6,5,4,3,2,1,0};
        for(int i=0;i<7;i++){
            int diff = a[i];
            long time = 24*60*60*1000*diff;
            Date newdate = new Date(date.getTime()-time);
            System.out.println(dateFormat.format(newdate));
            BigDecimal thisdaysales = sysOrderNewMapper.selectcompareTotalsales(diff);
            BigDecimal comdaysales = sysOrderNewMapper.selectcompareTotalsales(diff + 1);
            System.out.println("thisdaysales:"+thisdaysales);
            if(thisdaysales == null) {
                thisdaysales = new BigDecimal(0);
            }
                if (comdaysales == null){
                    comdaysales = new BigDecimal(0);
                }
                BigDecimal comparesales = thisdaysales.subtract(comdaysales);
                int thisdayordernum = sysOrderNewMapper.selectcompareTotalOrderNum(diff);
                int condayordernum = sysOrderNewMapper.selectcompareTotalOrderNum(diff + 1);
                int compareordernum = thisdayordernum - condayordernum;
                dayStatistical daliystatiscal = new dayStatistical();
                daliystatiscal.setDaysales(thisdaysales);
                daliystatiscal.setComsales(comparesales);
                daliystatiscal.setDaliyordernum(thisdayordernum);
                daliystatiscal.setComdaliyordernum(compareordernum);
                daliystatiscal.setDate(newdate);
                daliystatiscal.setNewdate(dateFormat.format(newdate));
                weeklystatical.add(daliystatiscal);
        }
        return weeklystatical;
    }

    @Override
    public List<OrderStatistical> getOrderStatistical(){
        List<OrderStatistical> orderStatisticals = new ArrayList<OrderStatistical>();
        BigDecimal TodayTotalSales = sysOrderNewMapper.selectTotalsales();
        System.out.println("TodayTotalSales:"+TodayTotalSales);
        BigDecimal ComTotalSales = sysOrderNewMapper.selectcompareTotalallsales(1);

        if(TodayTotalSales == null) {
            TodayTotalSales = new BigDecimal(0);
        }
        if (ComTotalSales == null){
            ComTotalSales = new BigDecimal(0);
        }
        BigDecimal com = TodayTotalSales.subtract(ComTotalSales);
        OrderStatistical orderstatistical = new OrderStatistical();
        orderstatistical.setSales(TodayTotalSales);
        orderstatistical.setTotalOrderNum(sysOrderNewMapper.selectTotalOrderNum());
        orderstatistical.setCustomerNum(sysOrderNewMapper.selectcustomerNum());
//        orderstatistical.setWeekStatistical(getWeeklyDayStatistical());
        orderstatistical.setComsales(com);
        orderStatisticals.add(orderstatistical);
        return orderStatisticals;
    }
}
