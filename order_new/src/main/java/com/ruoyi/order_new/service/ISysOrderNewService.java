package com.ruoyi.order_new.service;

import java.math.BigDecimal;
import java.util.List;

import com.ruoyi.order_new.domain.OrderStatistical;
import com.ruoyi.order_new.domain.SysOrderNew;
import com.ruoyi.order_new.domain.dayStatistical;

/**
 * 总订单Service接口
 * 
 * @author yeziyin
 * @date 2021-01-02
 */
public interface ISysOrderNewService 
{
    /**
     * 查询总订单
     * 
     * @param orderid 总订单ID
     * @return 总订单
     */
    public SysOrderNew selectSysOrderNewById(Long orderid);

    /**
     * 查询总订单列表
     * 
     * @param sysOrderNew 总订单
     * @return 总订单集合
     */
    public List<SysOrderNew> selectSysOrderNewList(SysOrderNew sysOrderNew);

    /**
     * 按openid查询历史所有订单
     *
     * @return 总订单集合
     */

    public List<SysOrderNew> selectAllSysOrderNewListByOpenid(String useropenid);
    /**
     * 按openid查询今天所有订单
     *
     * @return 总订单集合
     */

    public List<SysOrderNew> selectTodaySysOrderNewListByOpenid(String useropenid);

    /**
     * 新增总订单
     * 
     * @param sysOrderNew 总订单
     * @return 结果
     */
    public int insertSysOrderNew(SysOrderNew sysOrderNew);

    /**
     * 修改总订单
     * 
     * @param sysOrderNew 总订单
     * @return 结果
     */
    public int updateSysOrderNew(SysOrderNew sysOrderNew);

    /**
     * 批量删除总订单
     * 
     * @param orderids 需要删除的总订单ID
     * @return 结果
     */
    public int deleteSysOrderNewByIds(Long[] orderids);

    /**
     * 删除总订单信息
     * 
     * @param orderid 总订单ID
     * @return 结果
     */
    public int deleteSysOrderNewById(Long orderid);

    public BigDecimal getTotalSales();
    public List<dayStatistical> getWeeklyDayStatistical();

    public List<OrderStatistical> getOrderStatistical();
}
