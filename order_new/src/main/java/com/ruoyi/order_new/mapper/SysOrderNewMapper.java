package com.ruoyi.order_new.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.ruoyi.order_new.domain.SysOrderNew;

/**
 * 总订单Mapper接口
 * 
 * @author yeziyin
 * @date 2021-01-02
 */
public interface SysOrderNewMapper 
{
    /**
     * 查询总订单
     * 
     * @param orderid 总订单ID
     * @return 总订单
     */
    public SysOrderNew selectSysOrderNewById(Long orderid);

    /**
     * 查询总订单列表
     * 
     * @param sysOrderNew 总订单
     * @return 总订单集合
     */
    public List<SysOrderNew> selectSysOrderNewList(SysOrderNew sysOrderNew);

    /**
     * 按openid查询历史所有订单
     *
     * @return 总订单集合
     */
    public List<SysOrderNew> selectAllSysOrderNewListByOpenid(String useropenid);
    /**
     * 按openid查询今天所有订单
     *
     * @return 总订单集合
     */

    public List<SysOrderNew> selectTodaySysOrderNewListByOpenid(String useropenid);

    /**
     * 新增总订单
     * 
     * @param sysOrderNew 总订单
     * @return 结果
     */
    public int insertSysOrderNew(SysOrderNew sysOrderNew);

    /**
     * 修改总订单
     * 
     * @param sysOrderNew 总订单
     * @return 结果
     */
    public int updateSysOrderNew(SysOrderNew sysOrderNew);

    /**
     * 删除总订单
     * 
     * @param orderid 总订单ID
     * @return 结果
     */
    public int deleteSysOrderNewById(Long orderid);

    /**
     * 批量删除总订单
     * 
     * @param orderids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysOrderNewByIds(Long[] orderids);

    /**查询总营业额* */
    public BigDecimal selectTotalsales();


     /**查询总订单数* */
     public int selectTotalOrderNum();


/**查询当前相较前某天营业额* */
     public BigDecimal selectcompareTotalsales(int diff);

     public BigDecimal selectcompareTotalallsales(int diff);
    /**查询当前相较前某天的总订单数**/
    public int selectcompareTotalOrderNum(int diff);

 /**今日下单用户数**/
    public int selectcustomerNum();

}
