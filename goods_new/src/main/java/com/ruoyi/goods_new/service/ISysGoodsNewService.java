package com.ruoyi.goods_new.service;

import java.util.List;
import com.ruoyi.goods_new.domain.SysGoodsNew;

/**
 * 商品Service接口
 * 
 * @author yhh
 * @date 2020-12-25
 */
public interface ISysGoodsNewService 
{
    /**
     * 查询商品
     * 
     * @param goodsId 商品ID
     * @return 商品
     */
    public SysGoodsNew selectSysGoodsNewById(Long goodsId);

    /**
     * 查询商品列表
     * 
     * @param sysGoodsNew 商品
     * @return 商品集合
     */
    public List<SysGoodsNew> selectSysGoodsNewList(SysGoodsNew sysGoodsNew);

    /**
     * 新增商品
     * 
     * @param sysGoodsNew 商品
     * @return 结果
     */
    public int insertSysGoodsNew(SysGoodsNew sysGoodsNew);

    /**
     * 修改商品
     * 
     * @param sysGoodsNew 商品
     * @return 结果
     */
    public int updateSysGoodsNew(SysGoodsNew sysGoodsNew);

    /**
     * 批量删除商品
     * 
     * @param goodsIds 需要删除的商品ID
     * @return 结果
     */
    public int deleteSysGoodsNewByIds(Long[] goodsIds);

    /**
     * 删除商品信息
     * 
     * @param goodsId 商品ID
     * @return 结果
     */
    public int deleteSysGoodsNewById(Long goodsId);

    //    查找全部奶茶信息


    public List<SysGoodsNew> selectSysGoodsByMenuid(Long goodsInmenu);

    //获取该一种类的商品数量
    public int goodsnuminOneMenu(Long Menuid);

    //根据商品id获取商品图片
    public String selectSysGoodsImgById(Long goodsId);
}
