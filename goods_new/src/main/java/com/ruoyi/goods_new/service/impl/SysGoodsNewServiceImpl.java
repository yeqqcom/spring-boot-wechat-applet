package com.ruoyi.goods_new.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.goods_new.mapper.SysGoodsNewMapper;
import com.ruoyi.goods_new.domain.SysGoodsNew;
import com.ruoyi.goods_new.service.ISysGoodsNewService;

/**
 * 商品Service业务层处理
 * 
 * @author yhh
 * @date 2020-12-25
 */
@Service
public class SysGoodsNewServiceImpl implements ISysGoodsNewService 
{
    @Autowired
    private SysGoodsNewMapper sysGoodsNewMapper;

    /**
     * 查询商品
     * 
     * @param goodsId 商品ID
     * @return 商品
     */
    @Override
    public SysGoodsNew selectSysGoodsNewById(Long goodsId)
    {
        return sysGoodsNewMapper.selectSysGoodsNewById(goodsId);
    }

    /**
     * 查询商品列表
     * 
     * @param sysGoodsNew 商品
     * @return 商品
     */
    @Override
    public List<SysGoodsNew> selectSysGoodsNewList(SysGoodsNew sysGoodsNew)
    {
        return sysGoodsNewMapper.selectSysGoodsNewList(sysGoodsNew);
    }

    /**
     * 新增商品
     * 
     * @param sysGoodsNew 商品
     * @return 结果
     */
    @Override
    public int insertSysGoodsNew(SysGoodsNew sysGoodsNew)
    {
        return sysGoodsNewMapper.insertSysGoodsNew(sysGoodsNew);
    }

    /**
     * 修改商品
     * 
     * @param sysGoodsNew 商品
     * @return 结果
     */
    @Override
    public int updateSysGoodsNew(SysGoodsNew sysGoodsNew)
    {
        return sysGoodsNewMapper.updateSysGoodsNew(sysGoodsNew);
    }

    /**
     * 批量删除商品
     * 
     * @param goodsIds 需要删除的商品ID
     * @return 结果
     */
    @Override
    public int deleteSysGoodsNewByIds(Long[] goodsIds)
    {
        return sysGoodsNewMapper.deleteSysGoodsNewByIds(goodsIds);
    }

    /**
     * 删除商品信息
     * 
     * @param goodsId 商品ID
     * @return 结果
     */
    @Override
    public int deleteSysGoodsNewById(Long goodsId)
    {
        return sysGoodsNewMapper.deleteSysGoodsNewById(goodsId);
    }

    @Override
    public List<SysGoodsNew> selectSysGoodsByMenuid(Long goodsInmenu){return sysGoodsNewMapper.selectSysGoodsByMenuid(goodsInmenu);}

    @Override
    public int goodsnuminOneMenu(Long Menuid){
        return sysGoodsNewMapper.goodsnuminOneMenu(Menuid);
    }

    //根据商品id获取商品图片
    @Override
    public String selectSysGoodsImgById(Long goodsId){
        return sysGoodsNewMapper.selectSysGoodsImgById(goodsId);
    }
}
