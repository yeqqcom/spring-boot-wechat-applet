package com.ruoyi.goods_new.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品对象 sys_goods_new
 * 
 * @author yhh
 * @date 2020-12-25
 */
public class SysGoodsNew extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商品id */
    private Long goodsId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodsName;

    /** 商品图片 */
    @Excel(name = "商品图片")
    private String goodsImg;

    /** 商品上架状态 */
    @Excel(name = "商品上架状态")
    private String goodsState;

    /** 商品库存 */
    @Excel(name = "商品库存")
    private Long goodsStock;

    /** 商品价格 */
    @Excel(name = "商品价格")
    private BigDecimal goodsPrice;

    /** 商品简介 */
    @Excel(name = "商品简介")
    private String goodsDetail;

    /** 商品所属分类 */
    @Excel(name = "商品所属分类")
    private Long goodsInmenu;

    /** 商品所属菜单名称 */
    @Excel(name = "商品所属菜单名称")
    private String menuName;

    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setGoodsImg(String goodsImg) 
    {
        this.goodsImg = goodsImg;
    }

    public String getGoodsImg() 
    {
        return goodsImg;
    }
    public void setGoodsState(String goodsState) 
    {
        this.goodsState = goodsState;
    }

    public String getGoodsState() 
    {
        return goodsState;
    }
    public void setGoodsStock(Long goodsStock) 
    {
        this.goodsStock = goodsStock;
    }

    public Long getGoodsStock() 
    {
        return goodsStock;
    }
    public void setGoodsPrice(BigDecimal goodsPrice) 
    {
        this.goodsPrice = goodsPrice;
    }

    public BigDecimal getGoodsPrice() 
    {
        return goodsPrice;
    }
    public void setGoodsDetail(String goodsDetail) 
    {
        this.goodsDetail = goodsDetail;
    }

    public String getGoodsDetail() 
    {
        return goodsDetail;
    }
    public void setGoodsInmenu(Long goodsInmenu) 
    {
        this.goodsInmenu = goodsInmenu;
    }

    public Long getGoodsInmenu() 
    {
        return goodsInmenu;
    }
    public void setMenuName(String menuName) 
    {
        this.menuName = menuName;
    }

    public String getMenuName() 
    {
        return menuName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("goodsId", getGoodsId())
            .append("goodsName", getGoodsName())
            .append("goodsImg", getGoodsImg())
            .append("goodsState", getGoodsState())
            .append("goodsStock", getGoodsStock())
            .append("goodsPrice", getGoodsPrice())
            .append("goodsDetail", getGoodsDetail())
            .append("goodsInmenu", getGoodsInmenu())
            .append("menuName", getMenuName())
            .toString();
    }
}
