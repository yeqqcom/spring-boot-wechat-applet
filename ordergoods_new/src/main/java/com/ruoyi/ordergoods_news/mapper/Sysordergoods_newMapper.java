package com.ruoyi.ordergoods_news.mapper;

import java.util.List;
import com.ruoyi.ordergoods_news.domain.*;


/**
 * 订单详细商品Mapper接口
 * 
 * @author yhh
 * @date 2020-12-25
 */
public interface Sysordergoods_newMapper 
{
    /**
     * 查询订单详细商品
     * 
     * @param username 订单详细商品ID
     * @return 订单详细商品
     */
    public Sysordergoods_new selectSysordergoods_newById(String username);

    /**
     * 查询订单详细商品列表
     * 
     * @param sysordergoods_new 订单详细商品
     * @return 订单详细商品集合
     */
    public List<Sysordergoods_new> selectSysordergoods_newList(Sysordergoods_new sysordergoods_new);
/**
 * 根据订单号查询订单详细商品列表
 *
 * @param orderid 订单详细商品
 * @return 订单详细商品集合
 */
public List<Sysordergoods_new> selectSysordergoods_newListByorderid(Long orderid);

    /**
     * 新增订单详细商品
     * 
     * @param sysordergoods_new 订单详细商品
     * @return 结果
     */
    public int insertSysordergoods_new(Sysordergoods_new sysordergoods_new);

    /**
     * 修改订单详细商品
     * 
     * @param sysordergoods_new 订单详细商品
     * @return 结果
     */
    public int updateSysordergoods_new(Sysordergoods_new sysordergoods_new);

    /**
     * 删除订单详细商品
     * 
     * @param id 订单详细商品ID
     * @return 结果
     */
    public int deleteSysordergoods_newById(int id);

    /**
     * 批量删除订单详细商品
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysordergoods_newByIds(int[] ids);

    public Long findLastOrderId();
    public Long findLastPickId();
}

