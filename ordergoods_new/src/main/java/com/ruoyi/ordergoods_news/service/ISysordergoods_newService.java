package com.ruoyi.ordergoods_news.service;

import java.util.List;
import com.ruoyi.ordergoods_news.domain.Sysordergoods_new;
import com.ruoyi.ordergoods_news.domain.orderAllMassageList;
import com.ruoyi.ordergoods_news.domain.orderGoodsList;

/**
 * 订单详细商品Service接口
 * 
 * @author yhh
 * @date 2020-12-25
 */
public interface ISysordergoods_newService 
{
    /**
     * 查询订单详细商品
     * 
     * @param username 订单详细商品ID
     * @return 订单详细商品
     */
    public Sysordergoods_new selectSysordergoods_newById(String username);

    /**
     * 查询订单详细商品列表
     * 
     * @param sysordergoods_new 订单详细商品
     * @return 订单详细商品集合
     */
    public List<Sysordergoods_new> selectSysordergoods_newList(Sysordergoods_new sysordergoods_new);
    /**
     * 根据订单号查询订单详细商品列表
     *
     * @param orderid 订单详细商品
     * @return 订单详细商品集合
     */
    public List<orderGoodsList> selectSysordergoods_newListByorderid(Long orderid);
    /**
     * 新增订单详细商品
     * 
     * @param sysordergoods_new 订单详细商品
     * @return 结果
     */
    public int insertSysordergoods_new(Sysordergoods_new sysordergoods_new);

    /**
     * 修改订单详细商品
     * 
     * @param sysordergoods_new 订单详细商品
     * @return 结果
     */
    public int updateSysordergoods_new(Sysordergoods_new sysordergoods_new);

    /**
     * 批量删除订单详细商品
     * 
     * @param ids 需要删除的订单详细商品ID
     * @return 结果
     */
    public int deleteSysordergoods_newByIds(int[] ids);

    /**
     * 删除订单详细商品信息
     * 
     * @param id 订单详细商品ID
     * @return 结果
     */
    public int deleteSysordergoods_newById(int id);
    /**
     * 从小程序回传的信息，转换json存入数据库
     *
     * @param openid 用户openid
     * @param userName 用户昵称
     * @param goodsStr  订单的商品列表
     * @return 结果
     */
    public String addOneOrderByStr(String openid,String userName,String goodsStr,String Phone);
    /**
     * 根据openid获取该用户所有的历史订单
     *
     * @param openid 用户openid
     * @return 结果
     */
    public List<orderAllMassageList> getAllOrderListByOpenid(String openid);
    /**
     * 根据openid获取该用户所有的今日订单
     *
     * @param openid 用户openid
     * @return 结果
     */
    public List<orderAllMassageList> getTodayOrderListByOpenid(String openid);

}
