package com.ruoyi.ordergoods_news.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.customers.domain.SysCustomers;
import com.ruoyi.customers.mapper.SysCustomersMapper;
import com.ruoyi.customers.service.ISysCustomersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.ordergoods_news.mapper.Sysordergoods_newMapper;
import com.ruoyi.order_new.mapper.SysOrderNewMapper;
import com.ruoyi.goods_new.domain.SysGoodsNew;
import com.ruoyi.goods_new.mapper.SysGoodsNewMapper;
import com.ruoyi.ordergoods_news.domain.*;
import com.ruoyi.ordergoods_news.service.ISysordergoods_newService;
import com.ruoyi.order_new.domain.SysOrderNew;


/**
 * 订单详细商品Service业务层处理
 * 
 * @author yhh
 * @date 2020-12-25
 */
@Service
public class Sysordergoods_newServiceImpl implements ISysordergoods_newService
{
    @Autowired
    private Sysordergoods_newMapper sysordergoods_newMapper;

    @Autowired
    private  SysOrderNewMapper sysOrderNewMapper;

    @Autowired
    private  SysGoodsNewMapper sysgoodsnewMapper;

    @Autowired
    private SysCustomersMapper sysCustomersMapper;

    @Autowired
    private ISysCustomersService sysCustomersService;

    /**
     * 查询订单详细商品
     * 
     * @param username 订单详细商品ID
     * @return 订单详细商品
     */
    @Override
    public Sysordergoods_new selectSysordergoods_newById(String username)
    {
        return sysordergoods_newMapper.selectSysordergoods_newById(username);
    }

    /**
     * 查询订单详细商品列表
     * 
     * @param sysordergoods_new 订单详细商品
     * @return 订单详细商品
     */
    @Override
    public List<Sysordergoods_new> selectSysordergoods_newList(Sysordergoods_new sysordergoods_new)
    {
        return sysordergoods_newMapper.selectSysordergoods_newList(sysordergoods_new);
    }

    /**
     * 新增订单详细商品
     * 
     * @param sysordergoods_new 订单详细商品
     * @return 结果
     */
    @Override
    public int insertSysordergoods_new(Sysordergoods_new sysordergoods_new)
    {
        return sysordergoods_newMapper.insertSysordergoods_new(sysordergoods_new);
    }

    /**
     * 修改订单详细商品
     * 
     * @param sysordergoods_new 订单详细商品
     * @return 结果
     */
    @Override
    public int updateSysordergoods_new(Sysordergoods_new sysordergoods_new)
    {
        return sysordergoods_newMapper.updateSysordergoods_new(sysordergoods_new);
    }

    /**
     * 批量删除订单详细商品
     * 
     * @param ids 需要删除的订单详细商品ID
     * @return 结果
     */
    @Override
    public int deleteSysordergoods_newByIds(int[] ids)
    {
        return sysordergoods_newMapper.deleteSysordergoods_newByIds( ids );
    }

    /**
     * 删除订单详细商品信息
     *
     * @param id 订单详细商品ID
     * @return 结果
     */
    @Override
    public int deleteSysordergoods_newById(int id)
    {
        return sysordergoods_newMapper.deleteSysordergoods_newById(id);
    }
    /**
     * 从小程序回传的信息，转换json存入数据库
     *
     * @param openid 用户openid
     * @param userName 用户昵称
     * @param goodsStr  订单的商品列表
     * @return 结果
     */
    @Override
    public String addOneOrderByStr(String openid,String userName,String goodsStr,String Phone)
    {

        List<SysGoodsNew> updategoods = new ArrayList<SysGoodsNew>();
        String returnstr;
        SysCustomers thiscus = sysCustomersMapper.selectSysCustomersByOpenid(openid);
        System.out.println(thiscus.getCusState());
        if(thiscus.getCusState().equals("禁止下单")){
            returnstr = "该客户被禁止下单";
            return returnstr;
        }
        else{
//转对象集合
//        System.out.println(goodsStr);
            String res = JSON.toJSON(goodsStr).toString();
            List<orderGoodsList> ordergoodsLists = JSONArray.parseArray(res,orderGoodsList.class);
//        System.out.println(ordergoodsLists);
            //加入时间、订单编号以及取茶号
            Timestamp time=new Timestamp(System.currentTimeMillis());
            Long orderId=sysordergoods_newMapper.findLastOrderId()+1;
            Long pickId=sysordergoods_newMapper.findLastPickId()+1;
            Long goodsnums = (long) 0;
            Long tprice =(long)0;
            Sysordergoods_new orderEntry;
            SysGoodsNew updategood;

            //拆分drinklist,组成为单条条目，插入数据库
            for(orderGoodsList d:ordergoodsLists)
            {
                updategood= sysgoodsnewMapper.selectSysGoodsNewById(d.getDrinkid());
                Long goodsnowstock=updategood.getGoodsStock()-d.getDrinknum();
                updategood.setGoodsStock(goodsnowstock);
                if(goodsnowstock == 0){
                    updategood.setGoodsState("N");
                }
                updategoods.add(updategood);
                orderEntry=new Sysordergoods_new();
                orderEntry.setUseropenid(openid);
                orderEntry.setUsername(userName);
                orderEntry.setDrinkid(d.getDrinkid());
                orderEntry.setDrinkinfo(d.getDrinkinfo());
                orderEntry.setDrinkname(d.getDrinkname());
                orderEntry.setDrinknum(d.getDrinknum());
                orderEntry.setDrinkprice(d.getDrinkprice());
                orderEntry.setOrderid(orderId);
                orderEntry.setOrdertime(time);
                orderEntry.setPickid(pickId);
                tprice +=d.getDrinknum()*d.getDrinkprice();
                goodsnums += d.getDrinknum();
                if(sysordergoods_newMapper.insertSysordergoods_new(orderEntry)==0) {
                    returnstr ="下单失败";
                    return returnstr;
                }


            }
            BigDecimal totalprice = new BigDecimal(tprice);
            SysOrderNew ordernew = new SysOrderNew();
            ordernew.setUsername(userName);
            ordernew.setUseropenid(openid);
            ordernew.setOrderid(orderId);
            ordernew.setPickid(pickId);
            ordernew.setUserphone(Phone);
            ordernew.setGoodsAllnum(goodsnums);
            ordernew.setOrderallprice(totalprice);
            ordernew.setOrdertime(time);
            ordernew.setOrderfinishtype("F");
            if(sysOrderNewMapper.insertSysOrderNew(ordernew)==0){
                returnstr ="下单失败";
                return returnstr;
            }
            long point = totalprice.longValue();
            sysCustomersService.updateCustomerLever(openid, point);
            for(SysGoodsNew up:updategoods){
                sysgoodsnewMapper.updateSysGoodsNew(up);
            }
            returnstr ="下单成功";
            return returnstr;
        }

    }

    /**
     * 根据订单号查询订单详细商品列表
     *
     * @param orderid 订单详细商品
     * @return 订单详细商品集合
     */
    public List<orderGoodsList> selectSysordergoods_newListByorderid(Long orderid){
        List<Sysordergoods_new> ordergoods = sysordergoods_newMapper.selectSysordergoods_newListByorderid(orderid);
        List<orderGoodsList> ordergoodslists =new ArrayList<orderGoodsList>();
        for(Sysordergoods_new ordergood :ordergoods){
            orderGoodsList theorderGoods = new orderGoodsList();
            Long goodid = ordergood.getDrinkid();
            String img = sysgoodsnewMapper.selectSysGoodsImgById(goodid);
            theorderGoods.setDrinkid(goodid);
            theorderGoods.setDrinkname(ordergood.getDrinkname());
            theorderGoods.setDrinkimg(img);
            theorderGoods.setDrinkprice(ordergood.getDrinkprice());
            theorderGoods.setDrinknum(ordergood.getDrinknum());
            theorderGoods.setDrinkinfo(ordergood.getDrinkinfo());

            ordergoodslists.add(theorderGoods);
        }
        return ordergoodslists;
    }
    /**
     * 根据openid获取该用户所有的历史订单
     *
     * @param openid 用户openid
     * @return 结果
     */
    @Override
    public List<orderAllMassageList> getAllOrderListByOpenid(String openid){

        List<SysOrderNew> allorderbyopenid = sysOrderNewMapper.selectAllSysOrderNewListByOpenid(openid);
        List<orderAllMassageList> AllorderLists = new ArrayList<orderAllMassageList>();
        for(SysOrderNew order:allorderbyopenid){
            orderAllMassageList orderallmassage = new orderAllMassageList();
            Long orderid = order.getOrderid();
            orderallmassage.setOrderId(orderid);
            orderallmassage.setPickId(order.getPickid());
            orderallmassage.setTotalGoodsNum(order.getGoodsAllnum());
            orderallmassage.setTotalPrice(order.getOrderallprice());
            List<orderGoodsList> ordergoodslists = selectSysordergoods_newListByorderid(orderid);
            orderallmassage.setDrinkList(ordergoodslists);
            orderallmassage.setUserName(order.getUsername());
            orderallmassage.setOrderTime(new Timestamp(order.getOrdertime().getTime()));
            orderallmassage.setUserOpenid(order.getUseropenid());
            AllorderLists.add(orderallmassage);

        }
        return AllorderLists;
    }
    /**
     * 根据openid获取该用户所有的今日订单
     *
     * @param openid 用户openid
     * @return 结果
     */
    @Override
    public List<orderAllMassageList> getTodayOrderListByOpenid(String openid){
        List<SysOrderNew> allorderbyopenid = sysOrderNewMapper.selectTodaySysOrderNewListByOpenid(openid);
        List<orderAllMassageList> TodayorderLists = new ArrayList<orderAllMassageList>();
        for(SysOrderNew order:allorderbyopenid){
            orderAllMassageList orderallmassage = new orderAllMassageList();
            Long orderid = order.getOrderid();
            orderallmassage.setOrderId(orderid);
            orderallmassage.setPickId(order.getPickid());
            orderallmassage.setTotalGoodsNum(order.getGoodsAllnum());
            orderallmassage.setTotalPrice(order.getOrderallprice());
            List<orderGoodsList> ordergoodslists = selectSysordergoods_newListByorderid(orderid);
            orderallmassage.setDrinkList(ordergoodslists);
            orderallmassage.setUserName(order.getUsername());
            orderallmassage.setOrderTime(new Timestamp(order.getOrdertime().getTime()));
            orderallmassage.setUserOpenid(order.getUseropenid());
            TodayorderLists.add(orderallmassage);

        }
        return TodayorderLists;
    }

}
