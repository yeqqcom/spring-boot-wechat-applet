package com.ruoyi.ordergoods_news.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单详细商品对象 sys_ordergoods_new
 * 
 * @author yhh
 * @date 2020-12-25
 */
public class Sysordergoods_new extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 下单客户昵称 */
    @Excel(name = "下单客户昵称")
    private String username;
    /** 下单客户昵称 */
    @Excel(name = "序号")
    private int id;
    /** 下单客户openid */
    @Excel(name = "下单客户openid")
    private String useropenid;

    /** 商品id */
    @Excel(name = "商品id")
    private long drinkid;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String drinkname;

    /** 商品简介 */
    @Excel(name = "商品简介")
    private String drinkinfo;

    /** 商品价格 */
    @Excel(name = "商品价格")
    private Long drinkprice;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Long drinknum;

    /** 下单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "下单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date ordertime;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderid;

    /** 取餐码 */
    @Excel(name = "取餐码")
    private Long pickid;


    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }

    public void setUseropenid(String useropenid) 
    {
        this.useropenid = useropenid;
    }

    public String getUseropenid() 
    {
        return useropenid;
    }

    public void setDrinkid(long drinkid)
    {
        this.drinkid = drinkid;
    }
    public long getDrinkid()
    {
        return drinkid;
    }

    public void setDrinkname(String drinkname) 
    {
        this.drinkname = drinkname;
    }
    public String getDrinkname() 
    {
        return drinkname;
    }

    public void setDrinkinfo(String drinkinfo) 
    {
        this.drinkinfo = drinkinfo;
    }
    public String getDrinkinfo() 
    {
        return drinkinfo;
    }

    public void setDrinkprice(Long drinkprice) 
    {
        this.drinkprice = drinkprice;
    }
    public Long getDrinkprice() 
    {
        return drinkprice;
    }

    public void setDrinknum(Long drinknum) 
    {
        this.drinknum = drinknum;
    }
    public Long getDrinknum() 
    {
        return drinknum;
    }

    public void setOrdertime(Date ordertime) 
    {
        this.ordertime = ordertime;
    }
    public Date getOrdertime() 
    {
        return ordertime;
    }

    public void setOrderid(Long orderid) 
    {
        this.orderid = orderid;
    }
    public Long getOrderid() 
    {
        return orderid;
    }

    public void setPickid(Long pickid) 
    {
        this.pickid = pickid;
    }
    public Long getPickid() 
    {
        return pickid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("username", getUsername())
            .append("useropenid", getUseropenid())
            .append("drinkid", getDrinkid())
            .append("drinkname", getDrinkname())
            .append("drinkinfo", getDrinkinfo())
            .append("drinkprice", getDrinkprice())
            .append("drinknum", getDrinknum())
            .append("ordertime", getOrdertime())
            .append("orderid", getOrderid())
            .append("pickid", getPickid())
            .toString();
    }
}
