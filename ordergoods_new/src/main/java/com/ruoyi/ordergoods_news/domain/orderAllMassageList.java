package com.ruoyi.ordergoods_news.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public class orderAllMassageList {

        private String userName;
        private String userOpenid;
        private List<orderGoodsList> ordergoodsLists;
        private Timestamp orderTime;
        private long orderId;
        private long pickId;
        private BigDecimal totalPrice;
        private long totalGoodsNum;

        public orderAllMassageList() {
        }

        public orderAllMassageList(String userName, String userOpenid,List<orderGoodsList> ordergoodsLists, Timestamp orderTime, int orderId, int pickId, BigDecimal totalPrice,long totalgoodsnum) {
            this.userOpenid = userOpenid;
            this.userName = userName;
            this.ordergoodsLists = ordergoodsLists;
            this.orderTime = orderTime;
            this.orderId = orderId;
            this.pickId = pickId;
            this.totalPrice = totalPrice;
            this.totalGoodsNum=totalgoodsnum;

        }

        public orderAllMassageList(String userOpenid,String userName, List<orderGoodsList> ordergoodsLists, BigDecimal totalPrice) {
            this.userOpenid = userOpenid;
            this.userName = userName;
            this.ordergoodsLists = ordergoodsLists;
            this.totalPrice = totalPrice;
        }
        public String getUserOpenid() {
        return userOpenid;
    }

        public void setUserOpenid(String userOpenid) {
        this.userOpenid = userOpenid;
    }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public List<orderGoodsList> getDrinkList() {
            return ordergoodsLists;
        }

        public void setDrinkList(List<orderGoodsList> ordergoodsLists) {
            this.ordergoodsLists = ordergoodsLists;
        }

        public Timestamp getOrderTime() {
            return orderTime;
        }

        public void setOrderTime(Timestamp orderTime) {
            this.orderTime = orderTime;
        }

        public long getOrderId() {
            return orderId;
        }

        public void setOrderId(long orderId) {
            this.orderId = orderId;
        }

        public long getPickId() {
            return pickId;
        }

        public void setPickId(long pickId) {
            this.pickId = pickId;
        }

        public BigDecimal getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
        }

        public long getTotalGoodsNum() {
        return totalGoodsNum;
    }

        public void setTotalGoodsNum(long totalgoodsnum) {
        this.totalGoodsNum = totalgoodsnum;
    }


}
