package com.ruoyi.ordergoods_news.domain;


import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

public class orderGoodsList {

    /** 商品id **/
    private long drinkId;
    /** 商品名称 */
    private String drinkName;
    /** 商品图片 */
    private String drinkImg;
    /** 商品规格 */
    private String drinkInfo;
    /** 商品单价格 */
    private Long drinkPrice;
    /** 商品数量 */
    private Long drinkNum;
//    /** 商品总价 */
//    private Long goodstotalprice;
//    /** 订单id */
//    private Long orderid;

    public orderGoodsList (long drinkId, String drinkName,  Long drinkPrice, String drinkInfo, Long drinkNum,String drinkImg) {
        this.drinkId = drinkId;
        this.drinkName = drinkName;
        this.drinkNum = drinkNum;
        this.drinkInfo = drinkInfo;
        this.drinkPrice = drinkPrice;
        this.drinkImg = drinkImg;
    }

    public orderGoodsList(){

    }
    public long getDrinkid()
    {
        return drinkId;
    }
    public void setDrinkid(long drinkId)
    {
        this.drinkId = drinkId;
    }


    public void setDrinkname(String drinkName)
    {
        this.drinkName = drinkName;
    }
    public String getDrinkname()
    {
        return drinkName;
    }
    public String getDrinkimg()
    {
        return drinkImg;
    }

    public void setDrinkimg(String drinkImg)
    {
        this.drinkImg = drinkImg;
    }
    public void setDrinkprice(Long drinkPrice)
    {
        this.drinkPrice = drinkPrice;
    }
    public Long getDrinkprice()
    {
        return drinkPrice;
    }
    public void setDrinkinfo(String drinkInfo)
    {
        this.drinkInfo = drinkInfo;
    }
    public String getDrinkinfo()
    {
        return drinkInfo;
    }
    public void setDrinknum(Long drinkNum)
    {
        this.drinkNum = drinkNum;
    }
    public Long getDrinknum()
    {
        return drinkNum;
    }

//    public void setGoodstotalprice(Long goodstotalprice) {this.goodstotalprice = goodstotalprice;}
//    public Long getGoodstotalprice() {return  goodstotalprice;}
//
//    public void setOrderid(Long orderid)
//    {
//        this.orderid = orderid;
//    }
//    public Long getOrderid()
//    {
//        return orderid;
//    }



}
