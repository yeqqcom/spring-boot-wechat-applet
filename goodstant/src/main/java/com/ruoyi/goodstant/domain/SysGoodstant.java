package com.ruoyi.goodstant.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品规格对象 sys_goodstant
 * 
 * @author yhh
 * @date 2020-12-22
 */
public class SysGoodstant extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 规格编号 */
    @Excel(name = "规格编号")
    private Long stanId;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private Long goodsId;

    /** 规格名称 */
    @Excel(name = "规格名称")
    private String stanName;

    /** 规格值 */
    @Excel(name = "规格值")
    private String stanNum;

    public void setStanId(Long stanId) 
    {
        this.stanId = stanId;
    }

    public Long getStanId() 
    {
        return stanId;
    }
    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }
    public void setStanName(String stanName) 
    {
        this.stanName = stanName;
    }

    public String getStanName() 
    {
        return stanName;
    }
    public void setStanNum(String stanNum) 
    {
        this.stanNum = stanNum;
    }

    public String getStanNum() 
    {
        return stanNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("stanId", getStanId())
            .append("goodsId", getGoodsId())
            .append("stanName", getStanName())
            .append("stanNum", getStanNum())
            .toString();
    }
}
