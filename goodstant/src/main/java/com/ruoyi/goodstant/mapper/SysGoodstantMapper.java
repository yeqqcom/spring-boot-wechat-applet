package com.ruoyi.goodstant.mapper;

import java.util.List;
import com.ruoyi.goodstant.domain.SysGoodstant;

/**
 * 商品规格Mapper接口
 * 
 * @author yhh
 * @date 2020-12-22
 */
public interface SysGoodstantMapper 
{
    /**
     * 查询商品规格
     * 
     * @param stanId 商品规格ID
     * @return 商品规格
     */
    public SysGoodstant selectSysGoodstantById(Long stanId);

    /**
     * 查询商品规格列表
     * 
     * @param sysGoodstant 商品规格
     * @return 商品规格集合
     */
    public List<SysGoodstant> selectSysGoodstantList(SysGoodstant sysGoodstant);

    /**
     * 新增商品规格
     * 
     * @param sysGoodstant 商品规格
     * @return 结果
     */
    public int insertSysGoodstant(SysGoodstant sysGoodstant);

    /**
     * 修改商品规格
     * 
     * @param sysGoodstant 商品规格
     * @return 结果
     */
    public int updateSysGoodstant(SysGoodstant sysGoodstant);

    /**
     * 删除商品规格
     * 
     * @param stanId 商品规格ID
     * @return 结果
     */
    public int deleteSysGoodstantById(Long stanId);

    /**
     * 批量删除商品规格
     * 
     * @param stanIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysGoodstantByIds(Long[] stanIds);
}
