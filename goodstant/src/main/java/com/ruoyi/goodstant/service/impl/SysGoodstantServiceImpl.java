package com.ruoyi.goodstant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.goodstant.mapper.SysGoodstantMapper;
import com.ruoyi.goodstant.domain.SysGoodstant;
import com.ruoyi.goodstant.service.ISysGoodstantService;

/**
 * 商品规格Service业务层处理
 * 
 * @author yhh
 * @date 2020-12-22
 */
@Service
public class SysGoodstantServiceImpl implements ISysGoodstantService 
{
    @Autowired
    private SysGoodstantMapper sysGoodstantMapper;

    /**
     * 查询商品规格
     * 
     * @param stanId 商品规格ID
     * @return 商品规格
     */
    @Override
    public SysGoodstant selectSysGoodstantById(Long stanId)
    {
        return sysGoodstantMapper.selectSysGoodstantById(stanId);
    }

    /**
     * 查询商品规格列表
     * 
     * @param sysGoodstant 商品规格
     * @return 商品规格
     */
    @Override
    public List<SysGoodstant> selectSysGoodstantList(SysGoodstant sysGoodstant)
    {
        return sysGoodstantMapper.selectSysGoodstantList(sysGoodstant);
    }

    /**
     * 新增商品规格
     * 
     * @param sysGoodstant 商品规格
     * @return 结果
     */
    @Override
    public int insertSysGoodstant(SysGoodstant sysGoodstant)
    {
        return sysGoodstantMapper.insertSysGoodstant(sysGoodstant);
    }

    /**
     * 修改商品规格
     * 
     * @param sysGoodstant 商品规格
     * @return 结果
     */
    @Override
    public int updateSysGoodstant(SysGoodstant sysGoodstant)
    {
        return sysGoodstantMapper.updateSysGoodstant(sysGoodstant);
    }

    /**
     * 批量删除商品规格
     * 
     * @param stanIds 需要删除的商品规格ID
     * @return 结果
     */
    @Override
    public int deleteSysGoodstantByIds(Long[] stanIds)
    {
        return sysGoodstantMapper.deleteSysGoodstantByIds(stanIds);
    }

    /**
     * 删除商品规格信息
     * 
     * @param stanId 商品规格ID
     * @return 结果
     */
    @Override
    public int deleteSysGoodstantById(Long stanId)
    {
        return sysGoodstantMapper.deleteSysGoodstantById(stanId);
    }
}
